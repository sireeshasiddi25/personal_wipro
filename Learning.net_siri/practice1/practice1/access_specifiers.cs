﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice1
{
    internal class access_specifiers
    {
        public static void staticAccess()
        {
            My_base_class.staticdisplay();
        }
        static void main(string[] args)
        {
            My_base_class m1= new My_base_class();
            m1.printFieldValue();
           // m1.pub_field = 40;
           
            m1.setValue(100);
            m1.printFieldValue();
            Console.WriteLine("private value "+ m1.returnPrivateValue());
            

            Childclass c= new Childclass();
            c.childprintFieldValue();
            c.printFieldValue();
            c.accessProtectedMembers();


            access_specifiers.staticAccess();
        }
    }
}
