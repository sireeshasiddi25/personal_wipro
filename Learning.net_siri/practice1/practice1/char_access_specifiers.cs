﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice1
{
    internal class char_access_specifiers
    {
        public static void staticAccess()
        {
            char_base_class.staticdisplay();
        }
        static void main(string[] args)
        {
            char_base_class m1 = new char_base_class();
            m1.printFieldValue();
            // m1.pub_field = 40;

            m1.setValue('b');
            m1.printFieldValue();
            Console.WriteLine("private value " + m1.returnPrivateValue());


            stringChildClass c = new stringChildClass();
            c.childprintFieldValue();
            c.printFieldValue();
            c.accessProtectedMembers();


            char_access_specifiers.staticAccess();
        }
    }
}
