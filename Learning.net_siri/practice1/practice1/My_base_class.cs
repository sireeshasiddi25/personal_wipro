﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice1
{
    internal class My_base_class
    {
        private int a = 90;
        protected int pub_field = 10;

        public static int s = 34;

        public const int f = 59;

        public My_base_class()
        {
            
            
            //using constructor to use private variables 
            Console.WriteLine("Constructor in base class ");
        }
        public void printFieldValue()
        {
            Console.WriteLine("public variable "+pub_field);
        }
        public void setValue(int value)
        {
            Console.WriteLine("befor set value"+s);
            s = 26;
            Console.WriteLine("after set value"+s);
            pub_field = value;
        }
        protected void printStringValue()
        {
            Console.WriteLine("I'm in printStringValue method");
        }
        public int returnPrivateValue()
        {
            Console.WriteLine("returnPrivate value method");
            return a;
        }
        public static void staticdisplay()
        {
            s = 5;
            Console.WriteLine("static variable in static method " + s);
        }
    }
}
