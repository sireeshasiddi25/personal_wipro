﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice1
{
    class Childclass : My_base_class
    {
        //My_base_class mc1= new My_base_class();
        int b = 20;

        public  Childclass()
        {

            int sum = b + pub_field;
            Console.WriteLine("Constructor in child class : "+sum);
        }
        public void childprintFieldValue()
        {
            Console.WriteLine("base class method in child class ");

        }
        public void accessProtectedMembers()
        {
            printStringValue();
            Console.WriteLine("Im in access protected members method in child class");
        }


    }
}
