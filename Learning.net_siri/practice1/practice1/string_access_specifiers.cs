﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice1
{
    internal class string_access_specifiers
    {
        public static void staticAccess()
        {
            string_base_class.staticdisplay();
        }
        static void main(string[] args)
        {
            string_base_class m1 = new string_base_class();
            m1.printFieldValue();
            // m1.pub_field = 40;

            m1.setValue("trisha");
            m1.printFieldValue();
            Console.WriteLine("private value " + m1.returnPrivateValue());


            stringChildClass c = new stringChildClass();
            c.childprintFieldValue();
            c.printFieldValue();
            c.accessProtectedMembers();


            string_access_specifiers.staticAccess();
        }
    }
}

