﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice1
{
    internal class char_base_class
    {
        private char a ='a';
        protected char pub_field = 'b';

        public static char s = 'c';

        public const char f = 'd';

        public char_base_class()
        { 
            Console.WriteLine("Constructor in base class ");
        }
        public void printFieldValue()
        {
            Console.WriteLine("public variable " + pub_field);
        }
        public void setValue(char value)
        {
            Console.WriteLine("befor set value" + s);
            s = 'e';
            Console.WriteLine("after set value" + s);
            pub_field = value;
        }
        protected void printStringValue()
        {
            Console.WriteLine("I'm in printStringValue method ");
        }
        public char returnPrivateValue()
        {
            Console.WriteLine("returnPrivate value method ");
            return a;
        }
        public static void staticdisplay()
        {
            s = 'f';
            Console.WriteLine("static variable in static method " + s);
        }
    }
}
