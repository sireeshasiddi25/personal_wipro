﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice1
{
    internal class char_child_class : char_base_class
    {
        char b = 's';

        public char_child_class()
        {
            Console.WriteLine("Constructor in child class : " + b);
        }
        public void childprintFieldValue()
        {
            Console.WriteLine("base class method in child class ");

        }
        public void accessProtectedMembers()
        {
            printStringValue();
            Console.WriteLine("Im in access protected members method in child class");
        }
    }
}
