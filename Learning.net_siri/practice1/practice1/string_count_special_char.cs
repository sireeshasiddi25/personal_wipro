﻿using System;
namespace Practice1
{
	public class Special_symbols
	{
		public static void main(string[] args)
		{
			Console.WriteLine("enter your string");
			string s = Console.ReadLine();	
			Console.WriteLine("Length of string using length "+s.Length);
			Console.WriteLine("Length of string using count "+s.Count());
			int count = 0;
			char[] cb = new char[s.Length];
			int b = 0;
			for (int i = 0; i < s.Length; i++)
			{
				if ((s[i] >= 65 && s[i] <= 90) || (s[i] >= 48 && s[i] <= 57) || 
					(s[i] >= 97 && s[i] <= 122) || (s[i]==' '))
				{
					continue;
				}
				else
				{
					count += 1;
					cb[b] = s[i];
					b++;

				}

			}
            Console.WriteLine("Displaying special characters ");
            foreach (char j in cb)
            {
					Console.Write(j + " ");
            }

            char[] c= new char[s.Length];
            int a = 0;
			foreach(char ch in s) 
			{
				if(!char.IsLetterOrDigit(ch) && !char.IsWhiteSpace(ch))
				{
					c[a] = ch;
					a++;
				}
			}
			Console.WriteLine("Displaying special characters ");
			foreach(char j in c)
			{
				Console.Write(j+" ");
			}

            Console.WriteLine("No of special characters in a string " + count);
        }

	}

	
}
