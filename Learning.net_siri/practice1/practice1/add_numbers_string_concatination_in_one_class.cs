﻿using System;
namespace Practice1
{
    public class Practice
    {
        public int add(int a, int b)
        {
            return a + b;
        }
        public string stringConcat(string a, string b)
        {
            return a + " " + b;
        }
        public void stringConcat2(string a, string b)
        {
            Console.WriteLine(a + " " + b);
        }
        public static void main()
        {
            Practice p = new Practice();


            Console.WriteLine(p.add(300, 400));

            Console.WriteLine("enter 2 numbers in different line");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());


            Console.WriteLine(p.add(a, b));

            Console.WriteLine("enter 2 strings in different line");
            string s1 = Console.ReadLine();
            string s2 = Console.ReadLine();


            Console.WriteLine(p.stringConcat(s1, s2));

            p.stringConcat2(s1, s2);

            Console.WriteLine(p.stringConcat("hi", "siri"));

            p.stringConcat2("I'm", "good");

            string str1 = new string("siri");
            string str2 = new string('h', 5);
            Console.WriteLine(str1 + str2);


        }
    }
}