﻿using System;

namespace Practice1
{
    public class Class5
    {
        public int add(int a, int b)
        {
            return a + b;
        }
        public string stringConcat(string a, string b)
        {
            return a + " " + b;
        }
        public void stringConcat2(string a, string b)
        {
            Console.WriteLine(a + " " + b);
        }
    }

    public class Class4
    {
        public static void main()
        {
            Console.WriteLine("Hi Siri");
            Console.WriteLine("enter 2 numbers in different lines");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            Class5 c1 = new Class5();
            Console.WriteLine("Sum of the given numbers is :" + c1.add(a, b));
            int sum = c1.add(100, 398);
            Console.WriteLine(sum);

            Console.WriteLine("enter 2 strings");
            string s1 = Console.ReadLine();
            string s2 = Console.ReadLine();

            Console.WriteLine("String concatination :" + c1.stringConcat("I'm", "good"));
            Console.WriteLine("String concatination :" + c1.stringConcat(s1, s2));
            c1.stringConcat2("I'm siri", "and doing good");
        }
    }
}
