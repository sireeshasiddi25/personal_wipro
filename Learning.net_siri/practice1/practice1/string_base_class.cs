﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice1
{
    internal class string_base_class
    {
        private string a = "vani";
        protected string pub_field = "rupa";

        public static string s = "parvathi";

        public const string f = "sharan";

        public string_base_class()
        {


            //using constructor to use private variables 
            Console.WriteLine("Constructor in base class ");
        }
        public void printFieldValue()
        {
            Console.WriteLine("public variable " + pub_field);
        }
        public void setValue(string value)
        {
            Console.WriteLine("befor set value" + s);
            s = "navya";
            Console.WriteLine("after set value" + s);
            pub_field = value;
        }
        protected void printStringValue()
        {
            Console.WriteLine("I'm in printStringValue method ");
        }
        public string returnPrivateValue()
        {
            Console.WriteLine("returnPrivate value method ");
            return a;
        }
        public static void staticdisplay()
        {
            s = "shahid";
            Console.WriteLine("static variable in static method " + s);
        }
    }
}
