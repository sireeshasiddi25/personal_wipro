﻿using System;
namespace Practice1
{
	public class Class6
	{
		public static void main(string[] args)
		{
			Console.WriteLine("Hello");
			string str1 = new string("siri");
			string str2 = new string('h', 5);
			Console.WriteLine(str1 + str2);
			char[] chArr = {'s', 'i', 'r', 'i'};
			//we can use [] also
			string charString = new string(chArr);
			Console.WriteLine(charString);
		} 
	}
}
