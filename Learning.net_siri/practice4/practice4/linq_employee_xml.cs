﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace practice4
{
    class Employee2

    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Department { get; set; }
        public double Salary { get; set; }

    }


    class EmployeeManager2

    {
        public static List<string> filEmpSalary(IEnumerable<Employee2> employees,
            int minSalary,string dep)
        {
            var filterEmp = from Employee in employees
                            where Employee.Salary >= minSalary &&
                            Employee.Department== dep
                            orderby Employee.Name ascending
                            select Employee.Name;
            

            return filterEmp.ToList();
        }
    }

    
    class linq_employee_xml
    {
        static void main(string[] args)
        {

            string filePath = "C:\\Users\\Administrator\\sqlPractice\\" +
                "Product_testing\\employee.xml";
            XDocument doc = XDocument.Load(filePath);

            int minSalary = 2000000;
            var q = from employee in doc.Descendants("employee")
                    select new Employee2
                    {
                         Department=employee.Element("department").Value,
                         Name= employee.Element("name").Value,
                        Age = int.Parse(employee.Element("age").Value),
                        Salary = (int)employee.Element("salary")
                    };

            string dep = "human_Resouces";

            List<string> filteredNames = EmployeeManager2.filEmpSalary
                ((IEnumerable<Employee2>)q, minSalary,dep);


            
            foreach (var name in filteredNames)
            {
                Console.WriteLine(name);
            }
        }
    }
}

