﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace practice4
{
     class collections_xml_books
    {
        static void main(string[] args)
        {
            string filePath = "C:\\Users\\Administrator\\sqlPractice\\" +
                "Product_testing\\bookxml.xml";
            XDocument doc = XDocument.Load(filePath);


            var q = from book in doc.Descendants("book")
                    where (int)book.Element("year") >= 2000
                    orderby book.Element("title").Value descending
                    select new
                    {
                        Title = book.Element("title").Value,
                        Author = book.Element("author").Value,
                        Year = (int)book.Element("year")
                    };


            foreach (var book in q)
            {
                Console.WriteLine($"Title :{book.Title}, Author: {book.Author}, " +
                    $"Year: {book.Year}");
            }
        }
    }
}

