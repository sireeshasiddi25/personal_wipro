﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice4
{
    class Employee

    {
        public string Name { get; set; }
        public int Age { get; set; }
        public int Exp { get; set; }
        public double Salary { get; set; }

    }


    class EmployeeManager

    {
        public static List<string> filEmpBySalary(List<Employee> employees,
            double minSalary,int minExp)
        {
            var filterEmp = from Employee in employees
                             where Employee.Salary >= minSalary &&Employee.Exp>=minExp 
                             orderby Employee.Name ascending
                             select Employee.Name;


            return filterEmp.ToList();
        }
    }
        class linq_employee
    {
        static void main(string[] args)
        {
            List<Employee> employees = new List<Employee>()
        {
            new Employee() {Name = "Sharan", Age =42, Exp=21, Salary=400000},
            new Employee() {Name = "Mera", Age =32, Exp=11,Salary=200000},
            new Employee() {Name = "Sanskar", Age =36, Exp=15,Salary=300000},
            new Employee() {Name = "Shankar", Age =43, Exp=22,Salary=450000},
            new Employee() {Name = "Pooja", Age =38, Exp=17,Salary=350000}
        };


            double minSalary = 300000;
            int minExp = 20;
            List<string> filteredNames = EmployeeManager.filEmpBySalary
                (employees, minSalary,minExp);


            Console.WriteLine($"Employees with Salary above or equals to (minSalary) " +
                $"and experience above or equals to (minExp):");
            foreach (var name in filteredNames)
            {
                Console.WriteLine(name);
            }
        }
    }
}

