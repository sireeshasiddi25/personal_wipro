﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

namespace practice4
{
    class https_asynchronous
    {
        static async Task<string> ReadFileAsynchro(string path)
        {
            using (StreamReader sr = new StreamReader(path))
            {
                return await sr.ReadToEndAsync();
            }
        }
        static async Task<string> readDataAsync(string endPoint)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(endPoint);
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync();
            }

        }
        static async Task main(string[] args)
        {
            try
            {
                
               Task <string> data = ReadFileAsynchro
                    ("C:\\example_task\\example_task1\\README.md");
                Console.WriteLine("Data from file: " + data);
                

                Task<string> rd1 = readDataAsync
                    ("http://jsonplaceholder.tyecode.com/post1");
                Task<string> rd2 = readDataAsync
                    ("http://jsonplaceholder.tyecode.com/post1");

                string data3 = await data;


                Console.WriteLine("Data from file: " + data3);
                Console.WriteLine("Exceution Done");

                Console.WriteLine("Task 1 ID:" + rd1.Id);
                Console.WriteLine("Task 2 ID:" + rd2.Id);
                Console.WriteLine("Task 3 ID:" + data.Id);

                string data1 = await rd1;
                string data2 = await rd2;

                Console.WriteLine("Data fetched 1 " + data1);
                Console.WriteLine("Data fetched 2 " + data2);


            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred while reading the file:\n "
                    + ex.Message);
            }

        }
    }
}
