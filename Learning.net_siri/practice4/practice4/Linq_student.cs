﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice4
{
    class Student

    {
        public string Name { get; set; }
        public int Age { get; set; }
        public double Grade { get; set; }

    }


    class StudentManager

    {
        public static List<string> filStudByGrade(List<Student> students, 
            double minGrade)
        {
            var filterStud = from student in students
                             where student.Grade >= minGrade
                             orderby student.Name ascending
                             select student.Name;


            return filterStud.ToList();
        }

    }
    class Linq_student
    {
        static void main(string[] args)
        {
            List<Student> students = new List<Student>()
        {
            new Student() {Name = "Sharan", Age =21, Grade =85.5},
            new Student() {Name = "Mera", Age =21, Grade =85},
            new Student() {Name = "Sanskar", Age =21, Grade =85.7},
            new Student() {Name = "Shankar", Age =21, Grade =95.5},
            new Student() {Name = "Pooja", Age =21, Grade =86.5}
        };


            double minGrade = 85.0;
            List<string> filteredNames = StudentManager.filStudByGrade
                (students, minGrade);


            Console.WriteLine($"Students with grade above (minGrade):");
            foreach (var name in filteredNames)
            {
                Console.WriteLine(name);
            }
        }
    }
}
