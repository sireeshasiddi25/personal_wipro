﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice4
{
     class multiplication_multithreads
    {
        static  long[] number = new long[50];
        static int nChunks =5;
        static  int cSize = number.Length / nChunks;
        static long[] pro = new long[nChunks];
        static object lockObject = new object();

        static void CalculateProduct(int index)
        {
            
                int startInd = index * cSize;
                int endInd = (index + 1) * cSize;

                long product = 1;

                for (int i = startInd; i < endInd; i++)
                {
                    product *= number[i];
                }

                lock (lockObject)
                {
                    pro[index] = product;
                }
 

        }


        static void main()
        {
           
            for (int i = 0; i < number.Length; i++)
            {
                number[i] = i + 1;
            }
            foreach(int i in number)
            {
                Console.Write(i + "  ");
            }
            Thread[] threads = new Thread[nChunks];
            for (int i = 0; i < nChunks; i++)
            {
                int index = i;
                threads[i] = new Thread(() => CalculateProduct(index));
                threads[i].Start();
            }
            foreach (Thread thread in threads)
            {
                thread.Join();
            }
            long  totpro = 1;
            {
            foreach (int p in pro)
                totpro *= p;
            }
            Console.WriteLine("Total product: " + totpro);
            }

           
        }
    
}

