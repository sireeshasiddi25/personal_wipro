﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice4
{
     class multiplication_using_multithreads_exception_handling
    {
        static long[] number = new long[15];
        static int nChunks = 5;
        static int cSize = number.Length / nChunks;
        static long[] pro = new long[nChunks];
        static object lockObject = new object();

        static void CalculateProduct(int index)
        {
            try { 

            int startInd = index * cSize;
            int endInd = (index + 1) * cSize;

            long product = 1;
            for (int i = startInd; i < endInd; i++)
            {
                    if (index == 2)
                    {
                        
                        throw new InvalidOperationException("Trying to simulate " +
                            "the exception for this thread" +
                            Thread.CurrentThread.ManagedThreadId);
                        
                    }
                    if (i != 0)
                    {
                        product *= number[i];
                    }
                    else
                    {
                        Console.WriteLine("siri");
                    }
            }
               

                lock (lockObject)
                {
                    pro[index] = product;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("An error occured in thread " +
                    Thread.CurrentThread.ManagedThreadId + " " + ex.Message);
                
            }
            finally
            {
                Console.WriteLine("Thread " + Thread.CurrentThread.ManagedThreadId +
                    ": completed");
            }

        }

        static void main()
        {
            try
            {
                for (int i = 0; i < number.Length; i++)
                {
                    number[i] = i + 1;
                }
                foreach (int i in number)
                {
                    Console.Write(i + "  ");
                }
                Thread[] threads = new Thread[nChunks];
                for (int i = 0; i < nChunks; i++)
                {
                    int index = i;
                    threads[i] = new Thread(() => CalculateProduct(index));
                    threads[i].Start();
                }
                foreach (Thread thread in threads)
                {
                    thread.Join();
                }
                long totpro = 1;
                
                foreach (int p in pro)
                { if (p != 0)
                    {
                        totpro *= p;
                    }
                }
                Console.WriteLine("Total product: " + totpro);
            }

            catch (Exception ex)
            {
                Console.WriteLine("An error occured: " + ex.Message);
            }
        }
     }
}

    


