﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace practice4
{
     class asynchronous_https_user_post_comments
    {
        
        static async Task<string> readDataAsync(string endPoint)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(endPoint);
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync();
            }

        }
        static async Task main(string[] args)
        {
            try
            {

                Task<string> rd1 = readDataAsync
                    ("http://jsonplaceholder.typicode.com/posts");
                Task<string> rd2 = readDataAsync
                    ("http://jsonplaceholder.typicode.com/users");
                Task<string> rd3 = readDataAsync
                    ("http://jsonplaceholder.typicode.com/comments");

                

                Console.WriteLine("Task 1 ID:" + rd1.Id);
                Console.WriteLine("Task 2 ID:" + rd2.Id);
                Console.WriteLine("Task 3 ID:" + rd3.Id+"\n\n");
               

                string data1 = await rd1;
                string data2 = await rd2;
                string data4 = await rd3;

                Console.WriteLine("Data fetched 1 " + data1);
                Console.WriteLine("\n---------------\n");
                Console.WriteLine("Data fetched 2 " + data2);
                Console.WriteLine("\n---------------\n");
                Console.WriteLine("Data fetched 3 " + data4);


            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred while reading the file:\n "
                    + ex.Message);
            }

        }
    }
}
