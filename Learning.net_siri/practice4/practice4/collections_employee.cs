﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice4
{
    class Employee1

    {
        public string Name { get; set; }
        public int Age { get; set; }
        public int Exp { get; set; }
        public double Salary { get; set; }

    }
    class collections_employee
    {
        static void main(string[] args)
        {
            List<Employee> employees = new List<Employee>()
        {
            new Employee() {Name = "Sharan", Age =42, Exp=21, Salary=400000},
            new Employee() {Name = "Mera", Age =32, Exp=11,Salary=200000},
            new Employee() {Name = "Sanskar", Age =36, Exp=15,Salary=300000},
            new Employee() {Name = "Shankar", Age =43, Exp=22,Salary=450000},
            new Employee() {Name = "Pooja", Age =38, Exp=17,Salary=350000}
        };


            var output = employees.
                Where(Employee => Employee.Salary >= 300000 & Employee.Exp>=20)
                .OrderByDescending(Employee => Employee.Name)
                .Select(Employee => Employee);


            Console.WriteLine($"Employees with Salary above or equals to 300000 " +
                $"and experience above or equals to 20 :");
            foreach (var name in output)
            {
                Console.WriteLine("Name: "+ name.Name+" Age: "+name.Age+" Salary: "
                    +name.Salary+" Experience: "+name.Exp);
            }
        }
    }
}

