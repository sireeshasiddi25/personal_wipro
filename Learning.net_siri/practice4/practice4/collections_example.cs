﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Reflection.Metadata;

namespace practice4
{
     class collections_example
    {
        static void main(string[] args)
        {
            List<Item> collection = new List<Item>
        {
            new Item{ Name = "Item A", Property = 90},
            new Item{ Name = "Item B", Property = 20},
            new Item{ Name = "Item C", Property = 30},
            new Item{ Name = "Item D", Property = 40},
            new Item{ Name = "Item E", Property = 50},
            new Item{ Name = "Item F", Property = 60}
        };


            var q = collection.
                 Where(item => item.Property > 10)
                 .OrderByDescending(item => item.Property)
                 .Select(item => item);


            foreach (var item in q)
            {
                Console.WriteLine($"Name :{item.Name}, Property: {item.Property}");
            }
        }
    }
    class Item
    {
        public string Name { get; set; }
        public int Property { get; set; }
    }
}

