﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice4
{
    class asynchronous_with_exception_handling
    {
        static async Task<string> ReadFileAsynchro(string path)
        {
            using (StreamReader sr = new StreamReader(path))
            {
                return await sr.ReadToEndAsync();
            }
        }
        static async Task main(string[] args)
        {
            try 
            {
                //string data = await ReadFileAsynchro
                //("C:\\example_task\\example_task1\\README.md");
                string data = await ReadFileAsynchro
                    ("C:\\example_task\\example_task\\README.md");

                Console.WriteLine("Data from file: " + data);
            Console.WriteLine("Exceution Done");
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred while reading the file:\n "
                    +ex.Message);
            }
        }
    }
}

