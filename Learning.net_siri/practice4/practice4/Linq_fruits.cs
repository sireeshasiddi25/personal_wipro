﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice4
{
     class Linq_fruits
    {

        static void main(string[] args)
        {
            List<string> fruits = new List<string>()
        {
            "Apple", "Mango", "Banana", "Apricot", "Lime", "Orange", 
                "Water Melon", "Grapes", "Musk Melon"
        };


            var q = from fruit in fruits
                        //where fruit.EndsWith("o")
                        where fruit.Contains("A") || fruit.Contains("a")
                        //where (fruit.Contains("M") || fruit.Contains("m"))
                        //&& fruit.StartsWith("M")
                    //where fruit.StartsWith("M")
                    select fruit;


            foreach (var res in q)
            {
                Console.WriteLine(res);
            }
        }
    }
}
