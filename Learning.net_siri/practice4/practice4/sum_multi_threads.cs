﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice4
{
     class sum_multi_threads
    {
        static int[] number = new int[1000000];
        static int nChunks = 99;
        static int cSize = number.Length / nChunks;
        static int[] sums= new int[nChunks];
        static object lockObject = new object();

        static void CalculateSum(int index)
        {
            try
            {
                int startInd = index * cSize;
                int endInd = (index + 1) * cSize;

                int sum = 0;
                for (int i = startInd; i < endInd; i++)
                {
                    sum += number[i];
                }
                if (index == 2)
                {
                    throw new InvalidOperationException("Trying to simulate the exception for this thread" +
                        Thread.CurrentThread.ManagedThreadId);
                }
                lock (lockObject)
                {
                    sums[index] = sum;
                }
            }
            catch(Exception ex)
            { 
                Console.WriteLine("An error occured in thread "+
                    Thread.CurrentThread.ManagedThreadId+" "+ex.Message);
            }
            finally
            {
                Console.WriteLine("Thread " + Thread.CurrentThread.ManagedThreadId +
                    ": completed");
            }
                
        }
        static void main()
        {

            try
            {
                for (int i = 0; i < number.Length; i++)
                {
                    number[i] = i + 1;
                }
                Thread[] threads = new Thread[nChunks];
                for (int i = 0; i < nChunks; i++)
                {
                    int index = i;
                    threads[i] = new Thread(() => CalculateSum(index));
                    threads[i].Start();
                }
                foreach (Thread thread in threads)
                {
                    thread.Join();
                }
                int totSum = 0;
                foreach (int sum in sums)
                {
                    totSum += sum;
                }
                Console.WriteLine("Total sum: " + totSum);
            }
            catch(Exception ex)
            {
                Console.WriteLine("An error occured: " + ex.Message);
            }
        }

    }
}
