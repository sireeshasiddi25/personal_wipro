﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_practice1
{
    public class Employee
    {
        public string name;
        public string department;
        public decimal salary;

        public string Name
        {
            get { return name; }
            set
            {
                if (name != value)
                {
                    name = value;
                }
            }
        }

        public string Department
        {
            get { return department; }
            set
            {
                if (department != value)
                {
                    OnDepartmentChanged(value);
                    department = value;
                }
            }
        }

        public decimal Salary
        {
            get { return salary; }
            set
            {
                if (salary != value)
                {
                    OnSalaryChanged(value);
                    salary = value;
                }
            }
        }

        public event EventHandler<string> DepartmentChanged;
        public event EventHandler<decimal> SalaryChanged;

        protected virtual void OnDepartmentChanged(string newDepartment)
        {
            DepartmentChanged?.Invoke(this, newDepartment);
        }

        protected virtual void OnSalaryChanged(decimal newSalary)
        {
            SalaryChanged?.Invoke(this, newSalary);
        }
    }
}
