﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Legacy;

namespace test_practice1
{
    
        [TestFixture]
        public class add_mock_example
       {
            private Mock<Class1> addMock;
            [SetUp]
            public void Setup()
            {
                addMock = new Mock<Class1>();
            }
            [TearDown]
            public void Teardown()
            {
                addMock = null;
            }


            [Test]
            public void TestToAddPositiveNum()
            {
                addMock.Setup(x => x.add(It.IsAny<int>(),
                    It.IsAny<int>())).Returns(15);
                int result = addMock.Object.add(10, 5);


                ClassicAssert.AreEqual(result, 15);
            }


            [Test]
            public void TestToAddNegativeNum()
            {
                addMock.Setup(x => x.add(It.IsAny<int>(),
                    It.IsAny<int>())).Returns(-2);
                int result = addMock.Object.add(-5, 3);
            }

            [Test]
            public void TestAddNegativeNumNegative()
            {
                Class1 c = new Class1();
                ClassicAssert.AreEqual(-5, c.add(-3, -2));
            }


            [Test]
            public void TestAddPositiveNum()
            {
                addMock.Setup(x => x.add(It.IsAny<int>(), 
                    It.IsAny<int>())).Returns(70);
                int sum = addMock.Object.add(33, 37);
                if (sum != 70)
                {
                    throw new AssertionException($"Expected: {sum}," +
                        $" is not matching to actual");
                }
            }
        }
 }
    
    

