﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_practice1
{
    [TestFixture]
     class Salary_incremet_test
    {

        [Test]
        public static void CalSal()
        {

            Employee4 employee = new Employee4("Amarnath", 3000000m);
            PayrollSystem payrollSystem = new PayrollSystem();


            employee.SalaryChanged += payrollSystem.HandleSalaryChanged;


            employee.Salary = 40000000m;
        }
        [Test]
        public static void CalSal2()
        {

            Employee4 employee = new Employee4("Siri", 1000000m);
            PayrollSystem payrollSystem = new PayrollSystem();


            employee.SalaryChanged += payrollSystem.HandleSalaryChanged;


            employee.Salary = 10000000m;
        }
    }
}
