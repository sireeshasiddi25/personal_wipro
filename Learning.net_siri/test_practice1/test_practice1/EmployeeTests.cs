﻿using NUnit.Framework;
using NUnit.Framework.Legacy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_practice1
{
    [TestFixture]
    public class EmployeeTests
    {

        [Test]
        public void ListContainsCorrectEmployeesFromITDepartment()
        {
            List<Employee> employees = new List<Employee>
        {
            new Employee { Name = "John Doe", Department = "IT" },
            new Employee { Name = "Jane Smith", Department = "HR" },
            new Employee { Name = "Michael Johnson", Department = "IT" }
        };

            List<Employee> itEmployees = employees.Where
                (e => e.Department == "IT").ToList();
            foreach (var i in itEmployees)
            {
                Console.WriteLine($"name: {i.name}"+
                    $"department: {i.department}"+
                    $"salary: {i.salary}");
                CollectionAssert.AreEquivalent("IT", i.Department);
            }
        }
        [Test]
        public void DepartmentChangeEvent_IsTriggered()
        {

            Employee employee = new Employee();
            
            bool eventTriggered = false;
            employee.DepartmentChanged += (sender, args) => 
            eventTriggered = true;
            Console.WriteLine( $"department: {employee.department}" );

            employee.Department = "New Department";
            Console.WriteLine($"department: {employee.department}");

            ClassicAssert.IsTrue(eventTriggered);
            
        }

        [Test]
        public void SalaryChangeEvent_IsTriggered()
        {
            Employee employee = new Employee();
            bool eventTriggered = false;
            employee.SalaryChanged += (sender, args) => 
            eventTriggered = true;
            Console.WriteLine($"salary: {employee.salary}");
            employee.Salary = 50000;

            Console.WriteLine($"salary: {employee.salary}");
            ClassicAssert.IsTrue(eventTriggered);

        }

        [Test]
        public void EmployeeName_ContainsSubstring()
        {
            Employee employee = new Employee();
            Console.WriteLine($"name: {employee.name}");

           employee.Name = "John Doe";
            Console.WriteLine($"name: {employee.name}");
            StringAssert.Contains("Doe", employee.Name);
        }
    
    }
}