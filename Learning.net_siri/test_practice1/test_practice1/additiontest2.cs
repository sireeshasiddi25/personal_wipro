﻿using NUnit.Framework;
using NUnit.Framework.Legacy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_practice1
{
    [TestFixture]
    class additiontest2
    {
        [Test]
        public static void Cals()
        {
            Class1 c = new Class1();
            ClassicAssert.AreEqual(8,c.add(3, 5));
        }
        [Test]
        public static void CalNegatives()
        {
            Class1 c = new Class1();
            ClassicAssert.AreEqual(2,c.add(-3, 5));
        }
        [Test]
        public  void CalNegatives2()
        {
            Class1 c = new Class1();
            ClassicAssert.AreEqual(-8,c.add(-3, -5));
        }

    }
}


