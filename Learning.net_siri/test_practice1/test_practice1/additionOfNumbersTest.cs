﻿using NUnit.Framework;
using NUnit.Framework.Legacy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_practice1
{
    [TestFixture]
     class additionOfNumbersTest
    {
        [Test]
        public static void Cal()
        {
            Class1 c = new Class1();
            int sum = c.add(3, 8);
            if(sum!=11)
            {
                Assert.Fail("Sum value is nor correct: \n\n");
            }
            else
            {
                Assert.Pass("sum is correct\n\n");
            }
        }
        [Test]
        public static void CalNegative()
        {
            Class1 c = new Class1();
            int sum = c.add(3, -8);
            if (sum != -5)
            {
                Assert.Fail("Sum value is not correct: \n\n");
            }
            else
            {
                Assert.Pass("sum is correct\n\n");
            }
        }
        [Test]
        public static void CalNegative2()
        {
            Class1 c = new Class1();
            int sum = c.add(-3, -8);
            if (sum != -11)
            {

                Assert.Fail("Sum value is not correct: \n\n");
            }
            else
            {
                Assert.Pass("sum is correct\n\n");
            }
        }
       

    }

}

