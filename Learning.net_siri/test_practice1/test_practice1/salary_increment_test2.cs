﻿using NUnit.Framework;
using NUnit.Framework.Legacy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_practice1
{
    [TestFixture]
     class salary_increment_test2
    {
        [Test]
        public static void t1()
        {
            bool b = salaryincrement2.salinc(50000, 51000);
            if (b)
            { 
                Assert.Pass("incremented \n\n");
            }
            else
            {
                Assert.Fail("not incremented\n\n");
            }
        }
        [Test]
        public static void t2()
        {
            bool b = salaryincrement2.salinc(52000, 51000);
            if (!b)
            {
                Assert.Pass("not incremented\n\n");
            }
            else
            {
               
                Assert.Fail("salary incremented \n\n");
            }
        
        }
        [Test]
        public static void t6()
        {
            bool b = salaryincrement2.salinc(52000, 52000);
            if (!b)
            {
                Assert.Pass("not incremented\n\n");
            }
            else
            {

                Assert.Fail("salary incremented \n\n");
            }

        }
        [Test]
        public static void t3()
        {
            
            bool b = salaryincrement2.salinc(50000, 51000);
            ClassicAssert.IsTrue(b);

        }
        [Test]
        public static void t4()
        {
            
            bool b = salaryincrement2.salinc(52000, 51000);
            ClassicAssert.IsFalse(b);

        }
        [Test]
        public static void t5()
        {

            bool b = salaryincrement2.salinc(52000, 52000);
            ClassicAssert.IsFalse(b);

        }

    }
}
