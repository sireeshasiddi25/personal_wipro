﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice7
{
    class Employee
    {
        public string Name { get; set; }
        public string Department { get; set; }
        public double Salary { get; set; }
    }


    class Student
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Subject { get; set; }
    }


    class employee_queue
    {
        static void main(string[] args)
        {
            Queue<Employee> equeue = new Queue<Employee>();
            equeue.Enqueue(new Employee { Name = "Amarnath", 
                Department = "Management", Salary = 4000000 });
            equeue.Enqueue(new Employee { Name = "Dinesh",
                Department = "IT", Salary = 4000000 });
            equeue.Enqueue(new Employee { Name = "Rajinikanth", 
                Department = "Management", Salary = 4000000 });
            equeue.Enqueue(new Employee { Name = "Anil", 
                Department = "MobileDev", Salary = 4000000 });


            Console.WriteLine("Employess in office:");
            while (equeue.Count > 0)
            {
                Employee e = equeue.Dequeue();
                Console.WriteLine($"Name: {e.Name}, Department {e.Department}," +
                    $" Salary {e.Salary}");

            }
            Queue<Student> s = new Queue<Student>();
            s.Enqueue(new Student {Name = "Siri",Subject = "IT",Age =23});
            s.Enqueue(new Student { Name = "Rupa", Subject = "CSE", Age = 24 });
            s.Enqueue(new Student { Name = "Gouthu", Subject = "ECE", Age = 21 });



            Console.WriteLine("\n\nStudents in college:");
            while (s.Count > 0)
            {
                Student st = s.Dequeue();
                Console.WriteLine($"Name: {st.Name}, Subject {st.Subject}," +
                    $" Age {st.Age}");
            }
            
        }
    }

    
}
