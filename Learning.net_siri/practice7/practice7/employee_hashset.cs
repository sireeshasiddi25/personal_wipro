﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice7
{
    class employee_hashset
    {
        static void main(string[] args)
        {
            HashSet<Employee> equeue = new HashSet<Employee>();
            equeue.Add(new Employee
            {
                Name = "Amarnath",
                Department = "Management",
                Salary = 4000000
            });
            equeue.Add(new Employee
            {
                Name = "Dinesh",
                Department = "IT",
                Salary = 4000000
            });
            equeue.Add(new Employee
            {
                Name = "Rajinikanth",
                Department = "Management",
                Salary = 4000000
            });
            equeue.Add(new Employee
            {
                Name = "Anil",
                Department = "MobileDev",
                Salary = 4000000
            });


            Console.WriteLine("Employess in office:");
            foreach(var e in  equeue) 
            {
                //Employee e = equeue.Remove(i);
                Console.WriteLine($"Name: {e.Name}, Department {e.Department}," +
                    $" Salary {e.Salary}");

            }
            HashSet<Student> s = new HashSet<Student>();
            s.Add(new Student { Name = "Siri", Subject = "IT", Age = 23 });
            s.Add(new Student { Name = "Rupa", Subject = "CSE", Age = 24 });
            s.Add(new Student { Name = "Gouthu", Subject = "ECE", Age = 21 });



            Console.WriteLine("\n\nStudents in college:");
           foreach(var st  in s)
            {
                //Student st = s.Dequeue();
                Console.WriteLine($"Name: {st.Name}, Subject {st.Subject}," +
                    $" Age {st.Age}");
            }
        }
    }
}
