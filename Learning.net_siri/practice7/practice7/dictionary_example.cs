﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice7
{
     class dictionary_example
    {
        public static void main()
        {
            Dictionary<int,string> dic = new Dictionary<int,string>();

            dic.Add(1, "Siri");
            dic.Add(2, "Rupa");
            dic.Add(3, "Parvathi");
            dic.Add(4, "Navya");
            dic.Add(5, "Vani");

            string s = dic[3];
            Console.WriteLine(s);

            Console.WriteLine("enter a key value to check ");
            int i = Convert.ToInt32(Console.ReadLine());

            if(dic.ContainsKey(i))
            {

                Console.WriteLine($"value in index {i}: {dic[i]}");
            }

            foreach(KeyValuePair<int,string> kvp in dic)
            {
                Console.WriteLine(kvp.Key+" "+kvp.Value);
            }

            dic.Remove(i);
            Console.WriteLine($"after removing index {i}");
            foreach (KeyValuePair<int, string> kvp in dic)
            {
                Console.WriteLine(kvp.Key + " " + kvp.Value);
            }

            Console.WriteLine("values");
            foreach(var a in dic.Values)
            {
                Console.WriteLine(a);
            }
        }

    }
}
