﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice7
{
    public class Employee1
    {
        public string Name { get; set; }
        public string Department { get; set; }
        public int Id { get; set; }

        public Employee1(int id, string name, string dept)
        {
            Id = id;
            Name = name;
            Department = dept;
        }
    }

    class employee_dictionary
    {

        static void main(string[] args)
        {
            Employee1 emp1 = new Employee1(1, "Amarnath", "Manager");
            Employee1 emp2 = new Employee1(2, "Dinesh", "Developer");
            Employee1 emp3 = new Employee1(3, "Divya", "Associate Manager");
            Employee1 emp4 = new Employee1(4, "Rohini", "Director");



            Dictionary<int, Employee1> empDict = new Dictionary<int, Employee1>
        {
            { emp1.Id, emp1},
            { emp2.Id, emp2},
            { emp3.Id, emp3},
            { emp4.Id, emp4}
        };

            Console.WriteLine("enter number of employees u wanted to add");
            int a = Convert.ToInt32(Console.ReadLine());
            for(int i=0;i<a;i++) 
            {
                Console.Write("id of the employee:(id greater than 4) ");
                int b= Convert.ToInt32(Console.ReadLine());
                Console.Write("name of the employee: ");
                string c= Console.ReadLine();
                Console.WriteLine("department of an employee: ");
                string d=Console.ReadLine();
                Employee1 emp5 = new Employee1(b, c, d);
                empDict.Add(emp5.Id, emp5);
                
            }    


            Console.WriteLine("Employee Details");
            foreach (var kp in empDict)
            {
                Console.WriteLine($"Employee ID: {kp.Key}, " +
                    $"Employee Name: {kp.Value.Name}, Department: {kp.Value.Department}");
            }


            Console.WriteLine("Employee Details having dept manager");
            foreach (var kp in empDict)
            {
                if (kp.Value.Department == "Manager")
                {
                    Console.WriteLine($"Employee ID: {kp.Key}, " +
                        $"Employee Name: {kp.Value.Name}, Department: {kp.Value.Department}");
                }
            }

            Console.WriteLine("Employee Details having manager as substring in dept");
            foreach (var kp in empDict)
            {
                if (kp.Value.Department.Contains("Manager"))
                {
                    Console.WriteLine($"Employee ID: {kp.Key}, " +
                        $"Employee Name: {kp.Value.Name}, Department: {kp.Value.Department}");
                }
            }

        }
    }
}
