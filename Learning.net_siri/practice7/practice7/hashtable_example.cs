﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice7
{
     class hashtable_example
    {
        static void main(string[] args)
        {
            Hashtable hashtable = new Hashtable();


            hashtable.Add(1, "siri");
            hashtable.Add("2", "Rupa");
            hashtable.Add(3, "navya");
            hashtable.Add("4", "vani");
            hashtable.Add(5, "parvathi");
            hashtable.Add(6, "shahid");
            hashtable.Add(10, 1);
            hashtable.Add("20", 2);


            Console.WriteLine("Elements in hashtable: ");
            foreach (DictionaryEntry entry in hashtable)
            {
                Console.WriteLine("Key: " + entry.Key + ", value: " + entry.Value);
            }
        }
    }
}
