﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice7
{
     class hashset_example
    {
        static void main(string[] args)
        {
            HashSet<int> hSet = new HashSet<int>();
            hSet.Add(1);
            hSet.Add(2);
            hSet.Add(3);
            hSet.Add(4);
            hSet.Add(5);


            Console.WriteLine("Elements of the hash set:");


            foreach (int i in hSet)
            {
                Console.WriteLine(i + " ");
            }

        }
    }
}
