﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice7
{
     class singleton_example
    {
        private static singleton_example instance;


        private singleton_example()
        {


        }


        public static singleton_example GetInstance()
        {
            if (instance == null)
            {
                instance = new singleton_example();
            }
            return instance;
        }


        public void Sign()
        {
            Console.WriteLine("Singleton method");
        }
    }
    class Main1
    {
        static void main(string[] args)
        {
            singleton_example sign1 = singleton_example.GetInstance();
            singleton_example sign2 = singleton_example.GetInstance();


            Console.WriteLine("Are both of my instance same? " +
                (sign1 == sign2));


            sign1.Sign();
            sign2.Sign();
        }
    }
}
