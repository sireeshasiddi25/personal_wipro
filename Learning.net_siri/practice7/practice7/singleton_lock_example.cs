﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice7
{
    public class SignletonWithLock
    {
        private static SignletonWithLock instance;


        private static readonly object Lock = new object();


        private SignletonWithLock() { }


        public static SignletonWithLock GetInstance()
        {
            if (instance == null)
            {
                lock (Lock)
                {
                    if (instance == null)
                    {
                        instance = new SignletonWithLock();
                    }
                }
            }
            return instance;
        }


        public void myMethod()
        {
            Console.WriteLine("I am here");
        }
    }


    class singleton_lock_example
    {
        public static void main(string[] args)
        {
            SignletonWithLock swl1 = SignletonWithLock.GetInstance();
            SignletonWithLock swl2 = SignletonWithLock.GetInstance();


            Console.WriteLine("Are both instance same?: " + (swl1 == swl2));


            swl1.myMethod();
        }
    }
}
