﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice7
{
    class queue_example
    {
        static void main(string[] args)
        {
            Queue<int> num = new Queue<int>();
            num.Enqueue(1);
            num.Enqueue(2);
            num.Enqueue(10);
            num.Enqueue(20);

            foreach (int i in num)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine("Elements of the queue:");
            while (num.Count > 0)
            {
                Console.WriteLine(num.Dequeue() + " ");
            }
            Console.WriteLine("after dequeue");
            foreach (int i in num)
            {
                Console.WriteLine(i);
            }
        }
    }
}
