﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice7
{
     class stack_example
    {
        static void main(string[] args)
        {
            Stack<int> stack = new Stack<int>();
            stack.Push(1);
            stack.Push(2);
            stack.Push(30);
            stack.Push(100);
            foreach (int i in stack)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine("Elements of the stack:");


            while (stack.Count > 0)
            {
                Console.WriteLine(stack.Pop() + " ");
            }
        }

    }
}
