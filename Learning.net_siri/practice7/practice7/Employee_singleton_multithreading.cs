﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace practice7
{
    public class EmployeeManager
    {
        private static EmployeeManager instance = null;
        public static readonly object p = new object();
       
        public EmployeeManager() { }
        
        public static EmployeeManager Instance
        {
            get
            {
                lock(p)
                {
                    if(instance ==null)
                    {
                        instance = new EmployeeManager();
                    }
                    return instance;
                }
            }
        }
        public void add(int id,string name,string department)
        {
            /* Employee1 e=new Employee1 (id,name,department)
             { Id = id, Name = name, Department = department };

             Console.WriteLine($"Employee id {e.Id},name {e.Name}," +
                 $"Department {e.Department}");*/

            Console.WriteLine($"Employee id {id},name {name}," +
              $"Department {department}");
        }
       
    }
     class Employee_singleton_multithreading
    {
        public static void Main()
        {
            Console.WriteLine("enter number of employees u wanted to add");
            int a = Convert.ToInt32(Console.ReadLine());
            Thread[] t=new Thread[a];
            EmployeeManager m = EmployeeManager.Instance;
            for (int i = 0; i < t.Length; i++)
            {
                int id = i + 1;
                Console.WriteLine("enter name  of the employee: ");
                string c = Console.ReadLine();
                Console.WriteLine("department of an employee: ");
                string d = Console.ReadLine();
                t[i] = new Thread((employeeId) =>
                {

                    m.add(id, c, d);
                   
                });
                t[i].Start(id);
            }

            foreach(Thread th in t)
            { th.Join(); }

            
            Console.WriteLine("Done");
        }
    }
}
