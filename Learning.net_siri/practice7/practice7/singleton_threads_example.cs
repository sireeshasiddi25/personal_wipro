﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice7
{
     class singleton_threads_example
    {
        private static int counter = 0;
        private static readonly object lockObject = new object();


        static void main(string[] args)
        {
            Thread[] threads = new Thread[10];


            for (int i = 0; i < threads.Length; i++)
            {
                threads[i] = new Thread(IncrementCounter);
                threads[i].Start();
            }


            foreach (Thread thread in threads)
            {
                thread.Join();
            }


            Console.WriteLine("Final counter value: " + counter);
        }


        static void IncrementCounter()
        {
            for (int i = 0; i < 1000; i++)
            {
                lock (lockObject)
                {
                    counter++;
                }
            }
        }
    }
}
