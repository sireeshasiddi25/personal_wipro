﻿
namespace selenium_practice1.Test_util
{
    public class WebDriverOptions
    {

        public bool Headless { get; set; }
        public string Browser { get; set; }

    }
}
