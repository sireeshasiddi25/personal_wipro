﻿using OpenQA.Selenium;

namespace ecommerceProject1.Pages
{
     public class AddItemBySearchAndCatgory
    {
       public  WebDriver driver;
        public AddItemBySearchAndCatgory(WebDriver driver)
        {
            this.driver = driver;

        }

        public IWebElement bag1AddToCartMsg => driver.FindElement(By.XPath("//*[@id=\"maincontent\"]/div[1]/div[2]/div/div/div"));
        public IWebElement bag1addToCart => driver.FindElement(By.XPath("//*[@id=\"product-addtocart-button\"]"));
        public IWebElement bag1 => driver.FindElement(By.XPath("//*[@id=\"maincontent\"]/div[3]/div[1]/div[2]/div[2]/ol/li[2]/div/a/span/span/img"));
        public IWebElement searchText => driver.FindElement(By.XPath("//*[@id=\"search\"]"));
        public IWebElement createAccount => driver.FindElement(By.XPath("/html/body/div[2]/header/div[1]/div/ul/li[3]/a"));
        public IWebElement firstName =>driver.FindElement(By.Id("firstname"));
        public IWebElement lastName => driver.FindElement(By.Name("lastname"));
        public IWebElement email=>driver.FindElement(By.Name("email"));

        public IWebElement password => driver.FindElement(By.Name("password"));

        public IWebElement confirm_password => driver.FindElement(By.Name("password_confirmation"));
        public IWebElement create_acc_button => driver.FindElement(By.XPath("//*[@id=\"form-validate\"]/div/div[1]/button"));
        public IWebElement signin => driver.FindElement(By.XPath("/html/body/div[2]/header/div[1]/div/ul/li[2]/a"));

        public IWebElement email_signin => driver.FindElement(By.Id("email"));
        public IWebElement password_signin => driver.FindElement(By.Name("login[password]"));
        public IWebElement signin_button => driver.FindElement(By.Id("send2"));

        public IWebElement gear => driver.FindElement(By.XPath("//*[@id=\"ui-id-2\"]/li[4]"));
        public IWebElement gear1 => driver.FindElement(By.XPath("//*[@id=\"ui-id-6\"]"));
        public IWebElement watches => driver.FindElement(By.XPath("//*[@id=\"ui-id-27\"]"));
        public IWebElement watch1 => driver.FindElement(By.XPath("//*[@id=\"maincontent\"]/div[3]/div[1]/div[3]/ol/li[5]/div/a/span/span"));
        public IWebElement product1AddMessage => driver.FindElement(By.XPath("//*[@id=\"maincontent\"]/div[1]/div[2]/div/div/div"));
        public IWebElement watch11 => driver.FindElement(By.XPath("//*[@id=\"maincontent\"]/div[3]/div[1]/div[3]/ol/li[5]/div/a/span/span/img"));
        public IWebElement watch1_addcart => driver.FindElement(By.XPath("//*[@id=\"product-addtocart-button\"]"));
        public IWebElement cartSymbol=>driver.FindElement(By.XPath("/html/body/div[2]/header/div[2]/div[1]/a"));
        public IWebElement quantity_text => driver.FindElement(By.ClassName("item-qty cart-item-qty"));
        //public IWebElement quantity_text => driver.FindElement(By.XPath("//*[@id=\"cart-item-69809-qty\"]"));
        public IWebElement update_qty => driver.FindElement(By.XPath("//*[@id=\"update-cart-item-69809\"]"));

        public IWebElement proceedToCart => driver.FindElement(By.XPath("//*[@id=\"top-cart-btn-checkout\"]"));
        public IWebElement viewAndEditCart => driver.FindElement(By.XPath("//*[@id=\"minicart-content-wrapper\"]/div[2]/div[5]/div/a"));
        public IWebElement editQtyTxt => driver.FindElement(By.XPath("//*[@id=\"cart-71292-qty\"]"));
        public IWebElement updateCart => driver.FindElement(By.XPath("//*[@id=\"form-validate\"]/div[2]/button[2]"));
        public IWebElement usnamedropdown => driver.FindElement(By.XPath("/html/body/div[2]/header/div[1]/div/ul/li[2]/span/button"));
        public IWebElement delWatch => driver.FindElement(By.XPath("//*[@id=\"shopping-cart-table\"]/tbody/tr[2]/td/div/a[2]"));
        public IWebElement logout => driver.FindElement(By.XPath("/html/body/div[2]/header/div[1]/div/ul/li[2]/div/ul/li[3]/a"));
    
    }
}
