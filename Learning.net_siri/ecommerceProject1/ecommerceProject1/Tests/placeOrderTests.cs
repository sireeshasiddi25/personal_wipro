﻿using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using ecommerceProject1.Pages;
using Newtonsoft.Json;
using NUnit.Framework.Legacy;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using selenium_practice1.Test_util;



namespace ecommerceProject1.Tests
    {
        
        [TestFixture]
        class placeOrderTests
        {
            private IWebDriver driver;
            private PlaceOrderPage loginPage;

            public WebDriverUtility wd;

            public WebDriverWait wait;

            
            public ExtentReports extent;

            public ExtentTest test;

            [OneTimeSetUp]

            public void Setup()
            {

                // Initialize ExtentReports         

                var htmlReporter = new ExtentSparkReporter
                    (@"C:\Reports\ExtentReport.html");

                extent = new ExtentReports();

                extent.AttachReporter(htmlReporter);



            }


            [SetUp]

            public void BeforeTest()
            {
                WebDriverUtility.KillChromeDriverProcesses();
                driver = new ChromeDriver();

                test = extent.CreateTest
                    (TestContext.CurrentContext.Test.Name);

                loginPage = new PlaceOrderPage(driver);

                wd = new WebDriverUtility(driver);
                wd.NavigateToPage("https://magento.softwaretestingboard.com/");

            }


            [TearDown]

            public void AfterTest()
            {

                // Close the browser        

                driver.Quit();

                // End the test and add it to the report        

                extent.Flush();

            }


            [OneTimeTearDown]

            public void TearDown()
            {

                // Close the ExtentReports instance         

                extent.Flush();

            }

            public static IEnumerable<TestCaseData> GetTestData()
            {
                var currentDir = Directory.GetCurrentDirectory();

                var jsonFilePath = Path.Combine(currentDir,
                    "TestData\\credentials1.json");

                var jsonData = File.ReadAllText(jsonFilePath);

                var credentials = JsonConvert.DeserializeObject<List<UserCredentials>>(jsonData);

                foreach (var credential in credentials)
                {
                    Console.WriteLine($"Loaded credential: {credential.Username}, {credential.Password}");
                    yield return new TestCaseData(credential.Username, credential.Password);

                }

            }

            [Test, TestCaseSource("GetTestData")]
            public void clickItems(string Username, string Password)
            {
                loginPage.signin.Click();
                test.Log(Status.Pass, "signin click passed");
                loginPage.EnterUserName(Username);
                test.Log(Status.Pass, "enterusername passed");
                loginPage.EnterPassword(Password);
                test.Log(Status.Pass, "enter password passed");
                loginPage.SignInButton.Click();
                test.Log(Status.Pass, "signinbutton click passed");


                loginPage.Product.Click();
                test.Log(Status.Pass, "product click passed");
                loginPage.addelement.Click();
                test.Log(Status.Pass, "add element click passed");


                wd.setImplicitWait(TimeSpan.FromSeconds(20000));
                test.Log(Status.Pass, "implicit wait passed");
                ClassicAssert.IsTrue(loginPage.addproductmsg.Displayed, "no");
                test.Log(Status.Pass, " addproductmsg displayed passed");
                loginPage.homefrombag.Click();
                test.Log(Status.Pass, "homefrombag click passed");
                loginPage.scrollToProduct1();
                test.Log(Status.Pass, "scrolltoproduct passed");
                loginPage.mouseHover();
                test.Log(Status.Pass, "mouseHover passed");
                loginPage.product1color.Click();
                test.Log(Status.Pass, "product1color passed");
                loginPage.product1size.Click();
                test.Log(Status.Pass, "product1size passed");
                loginPage.mouseHover();
                test.Log(Status.Pass, "mousehover on product1 passed");
                loginPage.addproduct1.Click();
                test.Log(Status.Pass, " add product1 passed");

                loginPage.FluentWait();
                test.Log(Status.Pass, "fluentwait passed");
                 ClassicAssert.IsTrue(loginPage.product1addmsg.Displayed, "no");
                test.Log(Status.Pass, "  product1add msg displayed passed");

                loginPage.clickcart.Click();
                test.Log(Status.Pass, "clickcart  passed");
                wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10000));
                test.Log(Status.Pass, "wait passed");

                loginPage.editbagdetails.Click();
                test.Log(Status.Pass, "edibagdetails passed");
                loginPage.bagquantity.Click();
                test.Log(Status.Pass, "bagquantity click passed");
                loginPage.clearQuantityText();
                test.Log(Status.Pass, "clearQuantity text passed");
                loginPage.typeQuantityText("2");
                test.Log(Status.Pass, "typeQuantityText passed");
                loginPage.GetTextMethod();
                test.Log(Status.Pass, "getTextMethod passed");

                loginPage.updatebagcartbutton.Click();
                
                loginPage.proceedtocart.Click();
                test.Log(Status.Pass, "proceedtocart passed");
                test.Log(Status.Pass, "ClickItems passed");

            }
           
        }
    }


