﻿using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using ecommerceProject1.Pages;
using Newtonsoft.Json;
using NUnit.Framework.Legacy;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using selenium_practice1.Test_util;
using SeleniumExtras.WaitHelpers;

namespace ecommerceProject1.Tests
{
    [TestFixture]
     class paymentTest
    {
        private IWebDriver driver;
        private payment loginPage;

        public WebDriverUtility wd;

        public WebDriverWait wait;


        public ExtentReports extent;

        public ExtentTest test;

        [OneTimeSetUp]

        public void Setup()
        {

            // Initialize ExtentReports         

            var htmlReporter = new ExtentSparkReporter
                (@"C:\Reports\ExtentReport.html");

            extent = new ExtentReports();

            extent.AttachReporter(htmlReporter);



        }


        [SetUp]

        public void BeforeTest()
        {
            WebDriverUtility.KillChromeDriverProcesses();
            driver = new ChromeDriver();

            test = extent.CreateTest
                (TestContext.CurrentContext.Test.Name);

            loginPage = new payment(driver);

            wd = new WebDriverUtility(driver);
            wd.NavigateToPage("https://magento.softwaretestingboard.com/");

        }


        [TearDown]

        public void AfterTest()
        {

            // Close the browser        

            driver.Quit();

            // End the test and add it to the report        

            extent.Flush();

        }


        [OneTimeTearDown]

        public void TearDown()
        {

            // Close the ExtentReports instance         

            extent.Flush();

        }

        public static IEnumerable<TestCaseData> GetTestData()
        {
            var currentDir = Directory.GetCurrentDirectory();

            var jsonFilePath = Path.Combine(currentDir,
                "TestData\\credentials1.json");

            var jsonData = File.ReadAllText(jsonFilePath);

            var credentials = JsonConvert.DeserializeObject<List<UserCredentials>>(jsonData);

            foreach (var credential in credentials)
            {
                Console.WriteLine($"Loaded credential: {credential.Username}, {credential.Password}");
                yield return new TestCaseData(credential.Username, credential.Password);

            }

        }

        [Test, TestCaseSource("GetTestData")]
        public void SingleAddress(string Username, string Password)
        {
            loginPage.signin.Click();
            test.Log(Status.Pass, "signin click passed");
            loginPage.EnterUserName(Username);
            test.Log(Status.Pass, "enterusername passed");
            loginPage.EnterPassword(Password);
            test.Log(Status.Pass, "enter password passed");
            loginPage.SignInButton.Click();
            test.Log(Status.Pass, "signinbutton click passed");


            loginPage.Product.Click();
            test.Log(Status.Pass, "product click passed");
            loginPage.addelement.Click();
            test.Log(Status.Pass, "add element click passed");


            wd.setImplicitWait(TimeSpan.FromSeconds(20000));
            test.Log(Status.Pass, "implicit wait passed");
            ClassicAssert.IsTrue(loginPage.addproductmsg.Displayed, "no");
            test.Log(Status.Pass, " addproductmsg displayed passed");
            loginPage.homefrombag.Click();
            test.Log(Status.Pass, "homefrombag click passed");
            loginPage.scrollToProduct1();
            test.Log(Status.Pass, "scrolltoproduct passed");
            loginPage.mouseHover();
            test.Log(Status.Pass, "mouseHover passed");
            loginPage.product1color.Click();
            test.Log(Status.Pass, "product1color passed");
            loginPage.product1size.Click();
            test.Log(Status.Pass, "product1size passed");
            loginPage.mouseHover();
            test.Log(Status.Pass, "mousehover on product1 passed");
            loginPage.addproduct1.Click();
            test.Log(Status.Pass, " add product1 passed");

            loginPage.FluentWait();
            test.Log(Status.Pass, "fluentwait passed");
            ClassicAssert.IsTrue(loginPage.product1addmsg.Displayed, "no");
            test.Log(Status.Pass, "  product1add msg displayed passed");

            loginPage.clickcart.Click();
            test.Log(Status.Pass, "clickcart  passed");
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10000));
            test.Log(Status.Pass, "wait passed");

            loginPage.editbagdetails.Click();
            test.Log(Status.Pass, "edibagdetails passed");
            loginPage.bagquantity.Click();
            test.Log(Status.Pass, "bagquantity click passed");
            loginPage.clearQuantityText();
            test.Log(Status.Pass, "clearQuantity text passed");
            loginPage.typeQuantityText("2");
            test.Log(Status.Pass, "typeQuantityText passed");
            loginPage.GetTextMethod();
            test.Log(Status.Pass, "getTextMethod passed");

            loginPage.updatebagcartbutton.Click();
            test.Log(Status.Pass, "updatebagcartbutton passed");
            loginPage.proceedtocart.Click();
            test.Log(Status.Pass, "proceedtocart passed");
            loginPage.viewandeditcart.Click();
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id=\"checkout-step-shipping\"]/div[2]/button/span")));
            loginPage.newAddress.Click();
            test.Log(Status.Pass, "newAddress click passed");
            loginPage.fullNameAddress.SendKeys("siri");
            test.Log(Status.Pass, "fullNameAddress sendkeys passed");
            loginPage.lastNameAddress.SendKeys("siddi");
            test.Log(Status.Pass, "lastNameAddress sendkeys passed");
            loginPage.company.SendKeys("x");
            test.Log(Status.Pass, "company sendkeys passed");
            loginPage.streetAddress.SendKeys("Macherla");
            test.Log(Status.Pass, "streetAddress send keys passed");
            loginPage.city.SendKeys("palnadu");
            test.Log(Status.Pass, "city sendkeys passed");
            loginPage.state.Click();
            test.Log(Status.Pass, "state click passed");
            loginPage.state.SendKeys(Keys.Down);
            test.Log(Status.Pass, "state sendKeys passed");
            loginPage.country.Click();
            test.Log(Status.Pass, "country click passed");
            loginPage.country.SendKeys(Keys.Down);
            test.Log(Status.Pass, "country sendKeys passed");
            loginPage.zipCode.SendKeys("522426");
            test.Log(Status.Pass, "zipcode sendKeys passed");
            loginPage.phNo.SendKeys("1234567890");
            test.Log(Status.Pass, "phno sendKeys passed");
            if (loginPage.shipHere.Displayed)
            {
                loginPage.shipHere.Click();
                test.Log(Status.Pass, "shipHere click passed");
            }
            else
            {
                loginPage.shiNext.Click();
                test.Log(Status.Pass, "shiNext click passed");
            }
            

            test.Log(Status.Pass, "ClickItems passed");

        }

    }
}

