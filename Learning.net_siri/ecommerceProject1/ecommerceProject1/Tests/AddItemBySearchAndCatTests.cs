﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using ecommerceProject1.Pages;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using NUnit.Framework.Legacy;


namespace ecommerceProject1.Tests
{
    //remove products ,add product by searching and add product
    //by category
    [TestFixture]
     class AddItemBySearchAndCatTests
    {
        public WebDriver driver;
        public AddItemBySearchAndCatgory lo;
        public WebDriverWait wait;
        [SetUp]
        public void TestSetup()
        { 
            driver=new ChromeDriver();
            driver.Navigate().GoToUrl("https://magento.softwaretestingboard.com/");
            lo=new AddItemBySearchAndCatgory(driver);
        }
        [Test]
        public void create_account()
        {
            lo.createAccount.Click();
            lo.firstName.SendKeys("ramya");
            lo.lastName.SendKeys("y");
            lo.email.SendKeys("ramya@gmail.com");
            lo.password.SendKeys("ramya@2000");
            lo.confirm_password.SendKeys("ramya@2000");
            lo.create_acc_button.Click();

        }

        [Test]
        public void signin()
        {
            lo.signin.Click();
            lo.email_signin.SendKeys("ramya@gmail.com");
            lo.password_signin.SendKeys("ramy@2000");
            lo.signin_button.Click();
        }
        [Test]
        public void logout()
        {
            lo.signin.Click();
            lo.email_signin.SendKeys("sireeshasiddi25@gmail.com");
            lo.password_signin.SendKeys("Sirisha.nana25");
            lo.signin_button.Click();
            lo.usnamedropdown.Click();
            lo.logout.Click();
        }
        [Test]
        public void search()
        {
            lo.signin.Click();
            lo.email_signin.SendKeys("sireeshasiddi25@gmail.com");
            lo.password_signin.SendKeys("Sirisha.nana25");
            lo.signin_button.Click();
            lo.searchText.SendKeys("bag");
            lo.searchText.SendKeys(Keys.Return);
            lo.bag1.Click();
            lo.bag1addToCart.Click();
           
        }
        [Test]
        public void addProductAndIncQuantity()
        {
            Actions actions = new Actions(driver);

            actions.MoveToElement(lo.gear).Perform();
            lo.watches.Click();
            lo.watch1.Click();
            lo.watch1_addcart.Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            ClassicAssert.IsTrue(lo.product1AddMessage.Displayed, "no");
            lo.cartSymbol.Click();
           
        }
        [Test]
        public void addProductToCartAndEditQty()
        {
            Actions actions = new Actions(driver);

            actions.MoveToElement(lo.gear).Perform();
            lo.watches.Click();
            lo.watch1.Click();

            lo.watch1_addcart.Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            ClassicAssert.IsTrue(lo.product1AddMessage.Displayed, "no");
            lo.cartSymbol.Click();

        }
        [Test]
        public void removeItem()
        {
            Actions actions = new Actions(driver);

            actions.MoveToElement(lo.gear).Perform();
            lo.watches.Click();
            lo.watch1.Click();

            lo.watch1_addcart.Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            ClassicAssert.IsTrue(lo.product1AddMessage.Displayed, "no");
            lo.cartSymbol.Click();
            lo.viewAndEditCart.Click();
            lo.delWatch.Click();

        }



        [TearDown]
        public void TearDown()
        {
            //driver.Close();
            driver.Quit();
        }
    }
}
