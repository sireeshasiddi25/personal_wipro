﻿using ecommerceProject1.Pages;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using NUnit.Framework.Legacy;


namespace ecommerceProject1.Tests
{
    [TestFixture]
     class UserRegistrationTests
    {
        public WebDriver driver;
        public UserRegistration user;
       // public WebDriverWait wait;
        [SetUp]
        public void TestSetup()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://magento.softwaretestingboard.com/");
            user = new UserRegistration(driver);
        }
        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }

        [Test]
        public void CreateAnAccoutAccessibility()
        {
            user.CreateAccOnHome.Click();
            
        }
        [Test]
        public void MandatoryFieldInvalid()
        { 

            user.CreateAccOnHome.Click();
            user.firstName.SendKeys("sirisha");
            user.lastName.SendKeys("");
            user.email.SendKeys("sirisha25@gmail.com");
            user.password.SendKeys("Sirisha@25");
            user.confirm_password.SendKeys("Sirisha@25");
            user.create_acc_button.Click();
            ClassicAssert.IsTrue(user.lastNameRequired.Displayed, "Not displayed");

        }

        [Test]
        public void MandatoryFieldvalidation()
        {
            user.CreateAccOnHome.Click();
            user.firstName.SendKeys("sirisha");
            user.lastName.SendKeys("Siddi");
            user.email.SendKeys("sirisidd25@gmail.com");
            user.password.SendKeys("Sirisha@25");
            user.confirm_password.SendKeys("Sirisha@25");
            user.create_acc_button.Click();
            ClassicAssert.IsTrue(user.createMessage.Displayed, "not displayed");
        }
        [Test]
        public void MandatoryFieldInvalidfirstNAme()
        {
            user.CreateAccOnHome.Click();
            user.firstName.SendKeys("");
            user.lastName.SendKeys("Siddi");
            user.email.SendKeys("sirisha25@gmail.com");
            user.password.SendKeys("Sirisha@25");
            user.confirm_password.SendKeys("Sirisha@25");
            user.create_acc_button.Click();

            ClassicAssert.IsTrue(user.firstNameRequied.Displayed, "Not displayed");
        }
        [Test]
        public void MandatoryFieldInvalidEmail()
        {

            user.CreateAccOnHome.Click();
            user.firstName.SendKeys("sirisha");
            user.lastName.SendKeys("Siddi");
            user.email.SendKeys("");
            user.password.SendKeys("Sirisha@25");
            user.confirm_password.SendKeys("Sirisha@25");
            user.create_acc_button.Click();

            ClassicAssert.IsTrue(user.emailRequired.Displayed, "Not displayed");
        }

        [Test]
        public void MandatoryInvalidPassword()
        {

            user.CreateAccOnHome.Click();
            user.firstName.SendKeys("sirisha");
            user.lastName.SendKeys("Siddi");
            user.email.SendKeys("sirisga@gmail.com");
            user.password.SendKeys("");
            user.confirm_password.SendKeys("Sirisha@25");
            user.create_acc_button.Click();

            ClassicAssert.IsTrue(user.passwordRequired.Displayed, "Not displayed");
        }
        [Test]
        public void MandatoryInvalidConPassword()
        {

            user.CreateAccOnHome.Click();
            user.firstName.SendKeys("sirisha");
            user.lastName.SendKeys("Siddi");
            user.email.SendKeys("sirisga@gmail.com");
            user.password.SendKeys("");
            user.confirm_password.SendKeys("Sirisha@25");
            user.create_acc_button.Click();

            ClassicAssert.IsTrue(user.confirmPassRequired.Displayed, "Not displayed");
        }
        [Test]
        public void InvalidEmailFormat()
        {

            user.CreateAccOnHome.Click();
            user.firstName.SendKeys("sirisha");
            user.lastName.SendKeys("Siddi");
            user.email.SendKeys("sirisgagmail.com");
            user.password.SendKeys("Sirisha@25");
            user.confirm_password.SendKeys("Sirisha@25");
            user.create_acc_button.Click();

            ClassicAssert.IsTrue(user.invalidEmailFormat.Displayed, "Not displayed");
        }

        [Test]
        public void SpaceInEmail()
        {

            user.CreateAccOnHome.Click();
            user.firstName.SendKeys("sirisha");
            user.lastName.SendKeys("Siddi");
            user.email.SendKeys("sirisga @gmail.com");
            user.password.SendKeys("Sirisha@25");
            user.confirm_password.SendKeys("Sirisha@25");
            user.create_acc_button.Click();

            ClassicAssert.IsTrue(user.invalidEmailFormat.Displayed, "Not displayed");
        }

        [Test]
        public void PassAndConPassAreDiff()
        {

            user.CreateAccOnHome.Click();
            user.firstName.SendKeys("sirisha");
            user.lastName.SendKeys("Siddi");
            user.email.SendKeys("sirisga@gmail.com");
            user.password.SendKeys("Sirisha@25");
            user.confirm_password.SendKeys("Sirish@25");
            user.create_acc_button.Click();

            ClassicAssert.IsTrue(user.diffPassConPassMsg.Displayed, "Not displayed");
        }
        [Test]
        public void DuplicateEmail()
        {
            user.CreateAccOnHome.Click();
            user.firstName.SendKeys("sirisha");
            user.lastName.SendKeys("Siddi");
            user.email.SendKeys("sirisga@gmail.com");
            user.password.SendKeys("Sirisha@25");
            user.confirm_password.SendKeys("Sirisha@25");
            user.create_acc_button.Click();

            ClassicAssert.IsTrue(user.userExitsMsg.Displayed, "Not displayed");

        }
        [Test]
        public void WeakPassword()
        {

            user.CreateAccOnHome.Click();
            user.firstName.SendKeys("sirisha");
            user.lastName.SendKeys("Siddi");
            user.email.SendKeys("siriga@gmail.com");
            user.password.SendKeys("sirisha");
            user.confirm_password.SendKeys("sirisha");
            user.create_acc_button.Click();

            ClassicAssert.IsTrue(user.weakPasswordMsg.Displayed, "Not displayed");

        }
        [Test]
        public void SpecialCharacterInFirstName()
        {

            user.CreateAccOnHome.Click();
            user.firstName.SendKeys("sirisha@");
            user.lastName.SendKeys("Siddi");
            user.email.SendKeys("siriga@gmail.com");
            user.password.SendKeys("sirisha@25");
            user.confirm_password.SendKeys("sirisha@25");
            user.create_acc_button.Click();
            ClassicAssert.IsTrue(user.specialCharInFirstName.Displayed, "Not displayed");

        }
        [Test]
        public void SpecialCharacterInLastName()
        {

            user.CreateAccOnHome.Click();
            user.firstName.SendKeys("sirisha");
            user.lastName.SendKeys("Siddi@");
            user.email.SendKeys("siriga@gmail.com");
            user.password.SendKeys("sirisha@25");
            user.confirm_password.SendKeys("sirisha@25");
            user.create_acc_button.Click();

            ClassicAssert.IsTrue(user.SpecialCharInLastName.Displayed, "Not displayed");

        }


    }

 }

