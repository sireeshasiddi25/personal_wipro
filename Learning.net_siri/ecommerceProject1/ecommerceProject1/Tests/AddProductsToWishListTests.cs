﻿using ecommerceProject1.Pages;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;

using Newtonsoft.Json;
using NUnit.Framework.Legacy;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using selenium_practice1.Test_util;


namespace ecommerceProject1.Tests
{
    [TestFixture]
     class AddProductsToWishListTests
    {
        public WebDriver driver;
        public AddProductsToWishList user;
        public WebDriverWait wait;

        public WebDriverUtility wd;
        public ExtentReports extent;

        public ExtentTest test;

        [OneTimeSetUp]

        public void Setup()
        {

            // Initialize ExtentReports         

            var htmlReporter = new ExtentSparkReporter
                (@"C:\Reports\ExtentReport.html");

            extent = new ExtentReports();

            extent.AttachReporter(htmlReporter);



        }


        [SetUp]

        public void BeforeTest()
        {
            WebDriverUtility.KillChromeDriverProcesses();
            driver = new ChromeDriver();

            test = extent.CreateTest
                (TestContext.CurrentContext.Test.Name);

            user = new AddProductsToWishList(driver);

            wd = new WebDriverUtility(driver);
            wd.NavigateToPage("https://magento.softwaretestingboard.com/");

        }


        [TearDown]

        public void AfterTest()
        {

            // Close the browser        

            driver.Quit();

            // End the test and add it to the report        

            extent.Flush();

        }


        [OneTimeTearDown]

        public void TearDown()
        {

            // Close the ExtentReports instance         

            extent.Flush();

        }

        public static IEnumerable<TestCaseData> GetTestData()
        {
            var currentDir = Directory.GetCurrentDirectory();

            var jsonFilePath = Path.Combine(currentDir,
                "TestData\\credentials1.json");

            var jsonData = File.ReadAllText(jsonFilePath);

            var credentials = JsonConvert.DeserializeObject<List<UserCredentials>>(jsonData);

            foreach (var credential in credentials)
            {
                Console.WriteLine($"Loaded credential: {credential.Username}, {credential.Password}");
                yield return new TestCaseData(credential.Username, credential.Password);

            }

        }

        [Test, TestCaseSource("GetTestData")]
        
        public void addSingleProduct(string username,string password)
        {
            user.signin.Click();
            user.email.SendKeys(username);
            user.password.SendKeys(password);
            user.SignInButton.Click();

            user.product1.Click();
            user.Product1AddToWishList.Click();
            ClassicAssert.IsTrue(user.product1AddedMsg.Displayed, "no");
        }
        [Test, TestCaseSource("GetTestData")]
        public void addMultipleProducts(string username, string password)
        {
            user.signin.Click();
            user.email.SendKeys(username);
            user.password.SendKeys(password);
            user.SignInButton.Click();

            user.product1.Click();
            user.Product1AddToWishList.Click();
            ClassicAssert.IsTrue(user.product1AddedMsg.Displayed, "no");
            user.WhatsNew.Click();
            user.home.Click();
            user.Product2.Click();
            user.Product2WishList.Click();
            ClassicAssert.IsTrue(user.Product2AddMsg.Displayed, "no");

        }
    }
}
