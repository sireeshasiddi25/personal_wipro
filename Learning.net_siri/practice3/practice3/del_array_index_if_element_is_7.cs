﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice3
{
     class del_array_index_if_element_is_7
     {
        public static void main()
        {
            int[] num = new int[] { 1, 7, 8, 9, 7, 11, 12 };
            foreach (int i in num)
            {
                Console.Write(i + " ");
            }
            Console.WriteLine();
            int[] num2 = new int[num.Length];
            Console.WriteLine("enter index to remove");
            int b = Convert.ToInt32(Console.ReadLine());
            int j = 0;
            for (int i = 0; i < num.Length; i++)
            {
                if (!(i==b && num[b] == 7))
                {
                    num2[j] = num[i];
                    j++;
                }               
            }
            Console.WriteLine("New array");
            foreach (int i in num2)
            {
                Console.Write(i + " ");
            }
        }
     }
}
