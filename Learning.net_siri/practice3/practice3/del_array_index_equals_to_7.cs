﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice3
{
     class del_array_index_equals_to_7
    {
        public static void main()
        {
            int[] num = new int[] { 1, 7, 8, 9, 7, 11, 12 };
            int[] num2 = new int[num.Length];
            int j = 0;
            for(int i=0; i<num.Length; i++)
            {
                if (num[i] != 7)
                {
                    num2[j] = num[i];
                    j++;
                }
            }
            foreach(int i in num2)
            {
                Console.Write(i + " ");
            }
        }
    }
}
