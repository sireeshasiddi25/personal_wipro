﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice3
{
    class Person
    {
        private string name;
        private int age;
        public string Name
        {
            get { return name; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentNullException("Name cannot be null or empty");
                }
                name = value;
            }
        }
        public int Age
        {
            get { return age; }
            set
            {
                if (value <= 0 || value > 100)
                {
                    throw new ArgumentException("Age cannot be less tha 0 or " +
                        "greater than 100");
                }
                age = value;
            }
        }
        class get_set_try_throw_catch_finally
        {
            public static void main()
            {
                Person person = new Person();
                //person.Name = "sirisha";
                //person.Age = 23;
                try
                {
                    person.Name = ""
;                    Console.WriteLine("Name of the person " + person.Name);
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    person.Name = "Default";
                }

                try
                {
                    person.Age = 0;
                    Console.WriteLine("Age of the person " + person.Age);

                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    person.Age = 20;
                }

                Console.WriteLine("Name of the person " + person.Name);
                Console.WriteLine("Age of the person " + person.Age);

            }
        }
    }
}
