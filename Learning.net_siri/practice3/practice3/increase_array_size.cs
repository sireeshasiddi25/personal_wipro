﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice3
{
     class increase_array_size
    {
        public static void main()
        {
            char[] ch = new char[4];
            ch[0] = 's';
            ch[1] = 'i';
            ch[2] = 'r';
            ch[3] = 'i';
            

            foreach (char cs in ch)
            {
                Console.Write(cs + " ");
            }
            Console.WriteLine();

          
            int newsize = 10;
            char[] c=new char[newsize];
            int i = Math.Min(ch.Length, newsize);
            Array.Copy(ch, c, i);

            c[4] = 's';
            c[5] = 'h';
            c[6] = 'a';

            foreach (char cs in c)
            {
                Console.Write(cs + " ");
            }

        }

    }
}
