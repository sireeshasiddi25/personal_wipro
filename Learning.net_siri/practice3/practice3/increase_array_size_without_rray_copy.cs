﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice3
{
     class increase_array_size_without_array_copy
    {
        public static void main()
        {
            char[] arr1 = new char[5];
            char[] arr2 = new char[7];


            for (int i = 0; i < arr1.Length; i++)
            {
                Console.WriteLine("enter character");
                arr1[i] = char.Parse(Console.ReadLine());
            }

            foreach (char c in arr1)
            {
                Console.Write(c + " ");
            }

            Console.WriteLine();
            for (int i = 0; i < arr1.Length; i++)
            {
                arr2[i] = arr1[i];
            }
            for(int i=0;i<arr2.Length;i++)
            {
                if (i >= arr1.Length)
                {
                    Console.WriteLine("enter a character");
                    arr2[i] = char.Parse(Console.ReadLine());
                }
                
            }
            foreach(char c in arr2)
            {
                Console.Write(c + " ");
            }
        }
    
    }
}
