﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice3
{

    class Student1
    {
        private string name;
        private int age;
        public string Name
        {
            get { return name; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentNullException("Name cannot be null or empty");
                }
                name = value;
            }
        }
        public int Age
        {
            get { return age; }
            set
            {
                if (value <= 0 || value > 100)
                {
                    throw new ArgumentException("Age cannot be less tha 0 or " +
                        "greater than 100");
                }
                age = value;
            }
        }
        class switch_example1
        {
            public static void main()
            {
                Student1 s = new Student1();
                Console.WriteLine("enter number of student you wanted to add");
                int b = Convert.ToInt32(Console.ReadLine());
                List<string> n = new List<string>();
                List<int> a = new List<int>();
                for (int i = 0; i < b; i++)
                {
                    Console.WriteLine("enter name of student " + (i + 1));
                    s.Name = Console.ReadLine();
                    Console.WriteLine("enter age of student " + (i + 1));
                    s.Age = Convert.ToInt32(Console.ReadLine());
                    n.Add(s.Name);
                    a.Add(s.Age);
                }

                try
                {
                    //s.Name = "";
                    Console.WriteLine("Name of the person " + s.Name);
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    s.Name = "Default";
                }

                try
                {
                    //s.Age = 0;
                    Console.WriteLine("Age of the person " + s.Age);

                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    s.Age = 20;
                }

                Console.WriteLine("Name of the person " + s.Name);
                Console.WriteLine("Age of the person " + s.Age);

                Console.WriteLine("\n ------------");

                for (int i = 0; i < a.Count; i++)
                {
                    switch (a[i])
                    {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            Console.WriteLine("kg student");
                            Console.WriteLine(n[i]);

                            break;

                        case int x when (x < 15 && x > 5):
                            Console.WriteLine("class between 1 to 8");
                            Console.WriteLine(n[i]);

                            break;
                        case 15:
                            Console.WriteLine("class 9");
                            Console.WriteLine(n[i]);

                            break;
                        case 16:
                            Console.WriteLine("class 10");
                            Console.WriteLine(n[i]);

                            break;
                        default:
                            Console.WriteLine("not a school student");
                            Console.WriteLine(n[i]);

                            break;
                    }
                }
            }
        }
    }
}

                   
                       
