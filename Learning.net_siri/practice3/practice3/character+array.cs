﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice3
{
     class character_array
    {
        public static void main()
        {
            char[] ch = new char[10];
            ch[0] = 's';
            ch[1] = 'i';
            ch[2] = 'r';
            ch[3] = 'i';
            for (int i=0;i<ch.Length;i++)
            {
                Console.WriteLine("enter a character");
                ch[i] =char.Parse(Console.ReadLine());
            }

            foreach(char c in ch)
            {
                Console.Write(c + " ");
            }
        }
        
    }
}
