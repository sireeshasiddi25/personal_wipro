﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System.Collections.ObjectModel;
using System.Net;
using HtmlAgilityPack;
using OpenQA.Selenium.Chrome;
using System.Diagnostics;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;


namespace selenium_practice1.Test_util
{
    public class WebDriverUtility
    {
        private IWebDriver driver;


        public WebDriverUtility(IWebDriver driver)
        {
            this.driver = driver;
            //Console.WriteLine("WebDriverUtility constructor called");
        }

        public void NavigateToPage(string url)
        {
            driver.Navigate().GoToUrl(url);
            Console.WriteLine("WebDriverUtility Navigate to page called");

        }

        public IWebElement FindElement(By locator)
        {
            try
            {
                // Console.WriteLine("WebDriverUtility find element try called");
                return driver.FindElement(locator);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"error while trying to click on the element {locator}");
                Console.WriteLine(ex.Message);
                return null;
            }
        }


        public void ClickElement(By locator)
        {
            try
            {

                // Console.WriteLine("WebDriverUtility click element try called");
                FindElement(locator).Click();

            }
            catch (Exception ex)
            {
                Console.WriteLine($"error while trying to click on the element {locator}");
                Console.WriteLine(ex.Message);
                //return false;
            }
        }

        public ReadOnlyCollection<IWebElement> FindElements(By locator)
        {

            try
            {
                // Console.WriteLine("WebDriverUtility find elements try called");
                return driver.FindElements(locator);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"error while trying to click on the element {locator}");
                Console.WriteLine(ex.Message);
                return null;
            }

        }

        public void setImplicitWait(TimeSpan timespan)
        {
            try
            {
                // Console.WriteLine("WebDriverUtility set implicit wait try called");
                driver.Manage().Timeouts().ImplicitWait = timespan;
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);

            }

        }


        // Method to perform explicit wait
        public void WaitForElement(By locator, TimeSpan timeout)
        {
            try
            {
                //Console.WriteLine("WedDriverUtility wait for element called");
                var wait = new WebDriverWait(driver, timeout);
                wait.Until(ExpectedConditions.ElementExists(locator));
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error waiting for element with locator: {locator}");
                Console.WriteLine(ex.Message);
            }
        }

        // Method to perform fluent wait
        public void FluentWaitForElement(By locator, TimeSpan timeout,
            TimeSpan pollingInterval)
        {
            try
            {
                // Console.WriteLine("WedDriverUtility FluentWaitForElement called");
                var wait = new DefaultWait<IWebDriver>(driver)
                {
                    Timeout = timeout,
                    PollingInterval = pollingInterval
                };
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                wait.Until(ExpectedConditions.ElementExists(locator));
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error waiting for element with locator: {locator}");
                Console.WriteLine(ex.Message);
            }
        }

        // Method to perform a double click on an element
        public void DoubleClickElement(By locator)
        {
            try
            {
                //Console.WriteLine("WedDriverUtility DoubleClickElement called");
                var element = FindElement(locator);
                var actions = new Actions(driver);
                actions.DoubleClick(element).Build().Perform();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error double clicking on element with locator: {locator}");
                Console.WriteLine(ex.Message);
            }
        }

        // Method to scroll to an element
        public void ScrollToElement(By locator)
        {
            try
            {
                //Console.WriteLine("WedDriverUtility ScrollToElement called");
                var element = FindElement(locator);
                ((IJavaScriptExecutor)driver).ExecuteScript
                    ("arguments[0].scrollIntoView(true);", element);
                Console.WriteLine(element);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error scrolling to element with locator: " +
                    $"{locator}");
                Console.WriteLine(ex.Message);
            }
        }

        // Method to clear text from an input field
        public void ClearText(By locator)
        {
            try
            {
                FindElement(locator).Clear();//enabled,click into text,clear
                                             //text
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error clearing text from element with " +
                    $"locator: {locator}");
                Console.WriteLine(ex.Message);
            }
        }

        // Method to send keys to an input field
        public void SendKeysToElement(By locator, string text)
        {
            try
            {
                FindElement(locator).SendKeys(text);//enabled,click into
                                                    //text,clear text,send keys
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error sending keys to element with locator: {locator}");
                Console.WriteLine(ex.Message);
            }
        }


        // Method to get text of an element
        public string GetText(By locator)
        {
            try
            {
                return FindElement(locator).Text;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error getting text from element with " +
                    $"locator: {locator}");
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        // Method to get attribute value of an element
        public string GetAttributeValue(By locator, string attributeName)
        {
            try
            {
                return FindElement(locator).GetAttribute(attributeName);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error getting attribute value from " +
                    $"element with locator: {locator}");
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        // Method to check if an element is displayed
        public bool IsElementDisplayed(By locator)
        {
            try
            {
                return FindElement(locator).Displayed;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error checking if element is displayed " +
                    $"with locator: {locator}");
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        // Method to check if an element is enabled
        public bool IsElementEnabled(By locator)
        {
            try
            {
                return FindElement(locator).Enabled;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error checking if element is enabled with locator: {locator}");
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        // Method to check if an element is selected
        public bool IsElementSelected(By locator)
        {
            try
            {
                return FindElement(locator).Selected;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error checking if element is selected" +
                    $" with locator: {locator}");
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        // Method to navigate to a URL
        public void NavigateTo(string url)
        {
            try
            {
                driver.Navigate().GoToUrl(url);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error navigating to URL: {url}");
                Console.WriteLine(ex.Message);
            }
        }

        // Method to maximize the browser window
        public void MaximizeWindow()
        {
            try
            {
                driver.Manage().Window.Maximize();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error maximizing window: {ex.Message}");
            }
        }

        // Method to refresh the current page
        public void RefreshPage()
        {
            try
            {
                driver.Navigate().Refresh();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error refreshing page: {ex.Message}");
            }
        }

        // Method to get the title of the current page
        public string GetPageTitle()
        {
            try
            {
                return driver.Title;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error getting page title: {ex.Message}");
                return null;
            }
        }

        // Method to get the current URL
        public string GetCurrentURL()
        {
            try
            {
                return driver.Url;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error getting current URL: {ex.Message}");
                return null;
            }
        }

        // Method to switch to a frame by index
        public void SwitchToFrame(int index)
        {
            try
            {
                driver.SwitchTo().Frame(index);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error switching to frame by index {index}: {ex.Message}");
            }
        }

        // Method to switch to a frame by name or ID
        public void SwitchToFrame(string nameOrId)
        {
            try
            {
                driver.SwitchTo().Frame(nameOrId);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error switching to frame with name or " +
                    $"ID '{nameOrId}': {ex.Message}");
            }
        }

        // Method to switch to default content
        public void SwitchToDefaultContent()
        {
            try
            {
                driver.SwitchTo().DefaultContent();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error switching to default content: {ex.Message}");
            }
        }

        // Method to accept an alert
        public void AcceptAlert()
        {
            try
            {
                driver.SwitchTo().Alert().Accept();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error accepting alert: {ex.Message}");
            }
        }

        // Method to dismiss an alert
        public void DismissAlert()
        {
            try
            {
                driver.SwitchTo().Alert().Dismiss();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error dismissing alert: {ex.Message}");
            }
        }

        // Method to get text from an alert
        public string GetAlertText()
        {
            try
            {
                return driver.SwitchTo().Alert().Text;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error getting text from alert: {ex.Message}");
                return null;
            }
        }

        // Method to set a text in an alert
        public void SetAlertText(string text)
        {
            try
            {
                driver.SwitchTo().Alert().SendKeys(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error setting text in alert: {ex.Message}");
            }
        }

        // Method to drag and drop an element
        public void DragAndDropElement(By sourceLocator, By targetLocator)
        {
            try
            {
                var sourceElement = FindElement(sourceLocator);
                var targetElement = FindElement(targetLocator);
                var actions = new Actions(driver);
                actions.DragAndDrop(sourceElement, targetElement).
                    Build().Perform();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error dragging and dropping element from {sourceLocator} to {targetLocator}: {ex.Message}");
            }
        }

        // Method to hover over an element
        public void HoverOverElement(By locator)
        {
            try
            {
                var element = FindElement(locator);
                var actions = new Actions(driver);
                actions.MoveToElement(element).Build().Perform();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error hovering over element with locator: {locator}");
                Console.WriteLine(ex.Message);
            }
        }

        // Method to execute JavaScript
        public void ExecuteJavaScript(string script)
        {
            try
            {
                ((IJavaScriptExecutor)driver).ExecuteScript(script);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error executing JavaScript: {ex.Message}");
            }
        }

        // Method to scroll to the top of the page
        public void ScrollToTop()
        {
            try
            {
                ExecuteJavaScript("window.scrollTo(0, 0);");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error scrolling to the top of the page: {ex.Message}");
            }
        }

        // Method to scroll to the bottom of the page
        public void ScrollToBottom()
        {
            try
            {
                ExecuteJavaScript("window.scrollTo(0, " +
                    "document.body.scrollHeight);");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error scrolling to the bottom of the page: {ex.Message}");
            }
        }
        // Method to extract table rows and columns
        public List<List<string>> ExtractTableData(By tableLocator)
        {
            List<List<string>> tableData = new List<List<string>>();

            try
            {
                var table = driver.FindElement(tableLocator);

                var rows = table.FindElements(By.TagName("tr"));

                foreach (var row in rows)
                {
                    Console.WriteLine(row.Text);
                    var rowData = new List<string>();
                    var columns = row.FindElements(By.TagName("td"));

                    foreach (var column in columns)
                    {
                        Console.WriteLine(column.Text);
                        rowData.Add(column.Text);
                    }

                    tableData.Add(rowData);
                }
               

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error extracting table data: {ex.Message}");
            }

            return tableData;
        }

        // Method to extract values from table cells
        public string ExtractCellValue(By tableLocator, int rowIndex, int colIndex)
        {
            try
            {
                var table = driver.FindElement(tableLocator);
                var rows = table.FindElements(By.TagName("tr"));

                if (rowIndex < rows.Count)
                {
                    var columns = rows[rowIndex].FindElements(By.TagName("td"));

                    if (colIndex < columns.Count)
                    {
                        Console.WriteLine(columns[colIndex].Text);
                        return columns[colIndex].Text;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error extracting cell value: {ex.Message}");
            }

            return null;
        }

        // Method to dynamically identify table data
        public List<List<string>> IdentifyTableDataDynamicallyndReturnTable(By tableLocator, By rowDataLocator)
        {
            List<List<string>> tableData = new List<List<string>>();

            try
            {
                var table = driver.FindElement(tableLocator);
                var rows = table.FindElements(rowDataLocator);

                foreach (var row in rows)
                {
                    var rowData = new List<string>();
                    var columns = row.FindElements(By.XPath(".//td | .//th"));

                    foreach (var column in columns)
                    {
                        rowData.Add(column.Text);
                    }

                    tableData.Add(rowData);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error identifying table data dynamically: {ex.Message}");
            }

            return tableData;
        }

        // Method to dynamically identify row data and return row data
        public List<List<string>> IdentifyTableDataDynamically(By tableLocator, By rowDataLocator)
        {
            List<List<string>> tableData = new List<List<string>>();

            try
            {
                var table = driver.FindElement(tableLocator);
                var rows = table.FindElements(rowDataLocator);

                foreach (var row in rows)
                {
                    Console.WriteLine(row.Text);
                    var rowData = new List<string>();
                    var columns = row.FindElements(By.XPath(".//td | .//th"));

                    foreach (var column in columns)
                    {
                        Console.WriteLine(column.Text);
                        rowData.Add(column.Text);
                    }

                    tableData.Add(rowData);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error identifying table data dynamically: {ex.Message}");
            }

            return tableData;
        }

        // Method to handle dropdowns using Select class
        public void HandleDropdowns(By dropdownLocator, string value)
        {
            try
            {
                var dropdown = new SelectElement(driver.FindElement(dropdownLocator));
                dropdown.SelectByValue(value);
             
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error handling dropdown: {ex.Message}");
            }
        }

        // Method to handle multiple selections in dropdowns
        public void HandleMultipleSelections(By dropdownLocator, List<string> values)
        {
            try
            {
                var dropdown = new SelectElement(driver.FindElement(dropdownLocator));

                foreach (var value in values)
                {
                    dropdown.SelectByValue(value);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error handling multiple selections: {ex.Message}");
            }
        }

        // Method to scrape URLs from a website
        public static List<string> ScrapeUrls(string baseUrl)
        {
            List<string> urls = new List<string>();

            try
            {
                var web = new HtmlWeb();
                var doc = web.Load(baseUrl);

                // Select all anchor tags <a> with href attribute
                var links = doc.DocumentNode.SelectNodes("//a[@href]");

                if (links != null)
                {
                    foreach (var link in links)
                    {
                        string href = link.Attributes["href"].Value;

                        // Ensure the URL is absolute
                        Uri uri;
                        if (Uri.TryCreate(href, UriKind.Absolute, out uri))
                        {
                            urls.Add(href);
                        }
                        else
                        {
                            // Convert relative URL to absolute URL
                            Uri baseUri = new Uri(baseUrl);
                            uri = new Uri(baseUri, href);
                            urls.Add(uri.AbsoluteUri);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error scraping URLs: {ex.Message}");
            }

            return urls;
        }

        // Method to crawl a page and check if navigation is successful
        public static bool CrawlPage(string url)
        {
            try
            {
                var request = WebRequest.Create(url) as HttpWebRequest;
                request.Method = "HEAD"; // Use HEAD request to check page existence without downloading content
                var response = request.GetResponse() as HttpWebResponse;

                // Check if the response status code is a successful code (e.g., 200 OK)
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine($"Navigation successful for URL: {url}");
                    return true;
                }
                else
                {
                    Console.WriteLine($"Navigation failed for URL: {url}. HTTP status code: {response.StatusCode}");
                    // Add the failing URL to a list for further processing
                    AddFailingUrl(url);
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error accessing URL: {url} - {ex.Message}");
                // Add the failing URL to a list for further processing
                AddFailingUrl(url);
                return false;
            }
        }

        // Method to store failing URLs
        private static List<string> failingUrls = new List<string>();

        private static void AddFailingUrl(string url)
        {
            failingUrls.Add(url);
        }

        // Method to set page load timeout
        public void SetPageLoadTimeout(int timeout)
        {
            try
            {
                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(timeout);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error setting page load timeout: {ex.Message}");
            }
        }

        // Method to set script timeout
        public void SetScriptTimeout(TimeSpan timeout)
        {
            try
            {
                driver.Manage().Timeouts().AsynchronousJavaScript = timeout;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error setting script timeout: {ex.Message}");
            }
        }

        // Method to wait for Ajax-based components to load
        public void WaitForAjaxComplete(TimeSpan timeout)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, timeout);
                wait.Until(driver => (bool)
                (driver as IJavaScriptExecutor).ExecuteScript
                ("return jQuery.active == 0"));
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error waiting for Ajax complete:" +
                    $" {ex.Message}");
            }
        }

        // Method to handle simple alert
        public void HandleSimpleAlert()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                Console.WriteLine("Alert text: " + alert.Text);
                alert.Accept();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error handling simple alert: {ex.Message}");
            }
        }

        // Method to handle confirmation alert
        public void HandleConfirmationAlert(bool accept)
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                Console.WriteLine("Alert text: " + alert.Text);
                if (accept)
                    alert.Accept();
                else
                    alert.Dismiss();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error handling confirmation alert: {ex.Message}");
            }
        }

        // Method to handle prompt alert
        public void HandlePromptAlert(string inputText, bool accept)
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                Console.WriteLine("Alert text: " + alert.Text);
                if (inputText != null)
                    alert.SendKeys(inputText);
                if (accept)
                    alert.Accept();
                else
                    alert.Dismiss();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error handling prompt alert: {ex.Message}");
            }
        }

        // Method to navigate to a URL
        public void NavigateToUrl(string url)
        {
            try
            {
                driver.Navigate().GoToUrl(url);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error navigating to URL: {ex.Message}");
            }
        }

        // Method to close the WebDriver instance
        public void CloseWebDriver()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error closing WebDriver: {ex.Message}");
            }
        }



        // Method to handle prompt alert



        public static void WaitForAjax(IWebDriver driver,
            int timeoutInSeconds)
        {
            if (timeoutInSeconds > 0)
            {
                WebDriverWait wait = new WebDriverWait(driver,
                    TimeSpan.FromSeconds(timeoutInSeconds));
                wait.Until(d =>
                {
                    return (bool)(driver as IJavaScriptExecutor).ExecuteScript("return jQuery.active == 0");
                });
            }
        }

        public static void SelectDate(IWebDriver driver,
            By calendarElement, string date)
        {
            IWebElement calendar = driver.FindElement(calendarElement);
            calendar.Click();

            IWebElement dateElement = calendar.FindElement(By.XPath($"//td[text()='{date}']"));
            dateElement.Click();
        }

        public static List<string> GetTableDataWithPagination(IWebDriver driver, By tableLocator, By nextPageButtonLocator)
        {
            List<string> tableData = new List<string>();
            bool isNextPage = true;

            while (isNextPage)
            {
                IWebElement table = driver.FindElement(tableLocator);
                var rows = table.FindElements(By.TagName("tr"));
                foreach (var row in rows)
                {
                    tableData.Add(row.Text);
                }

                try
                {
                    IWebElement nextPageButton = driver.FindElement(nextPageButtonLocator);
                    nextPageButton.Click();
                }
                catch (NoSuchElementException)
                {
                    isNextPage = false;
                }
            }

            return tableData;
        }

        public static IWebElement GetShadowRoot(IWebDriver driver, IWebElement element)
        {
            return (IWebElement)((IJavaScriptExecutor)driver).ExecuteScript("return arguments[0].shadowRoot", element);
        }

        public static IWebElement FindElementInShadowRoot(IWebDriver driver, IWebElement shadowHost, By locator)
        {
            IWebElement shadowRoot = GetShadowRoot(driver, shadowHost);
            return shadowRoot.FindElement(locator);
        }

        public static void SwitchToFrame(IWebDriver driver, By frameLocator)
        {
            IWebElement frameElement = driver.FindElement(frameLocator);
            driver.SwitchTo().Frame(frameElement);
        }

        public static void SwitchToMainContent(IWebDriver driver)
        {
            driver.SwitchTo().DefaultContent();
        }

        public static string GetPseudoElementContent(IWebDriver driver, IWebElement element, string pseudoElement)
        {
            return (string)((IJavaScriptExecutor)driver).ExecuteScript(
                "return window.getComputedStyle(arguments[0], arguments[1]).getPropertyValue('content');",
                element, pseudoElement);
        }


        // <summary>
        /// Tries to find and interact with an element, handling StaleElementReferenceException by retrying.
        /// </summary>
        /// <param name="driver">The WebDriver instance.</param>
        /// <param name="locator">The locator for the element to interact with.</param>
        /// <param name="action">The action to perform on the element.</param>
        /// <param name="retryCount">The number of retry attempts.</param>
        public static void PerformActionWithRetry(IWebDriver driver, By locator, Action<IWebElement> action, int retryCount = 3)
        {
            for (int attempt = 0; attempt < retryCount; attempt++)
            {
                try
                {
                    IWebElement element = driver.FindElement(locator);
                    action(element);
                    return;
                }
                catch (StaleElementReferenceException)
                {
                    if (attempt == retryCount - 1)
                    {
                        throw;
                    }
                    // Optionally, wait a bit before retrying
                    System.Threading.Thread.Sleep(500);
                }
            }
        }
        // Initialize WebDriver with options
        public static IWebDriver InitializeDriver(WebDriverOptions options)
        {
            try
            {
                if (options.Browser == "Chrome")
                {
                    ChromeOptions chromeOptions = new ChromeOptions();
                    chromeOptions.AddArgument("--disable-extensions");
                    chromeOptions.AddArgument("--start-maximized");
                    //chromeOptions.AddArgument("--headless");

                    if (options.Headless)
                    {
                        chromeOptions.AddArgument("--headless");
                    }
                    return new ChromeDriver(chromeOptions);
                }

                else if (options.Browser == "Edge")
                {
                    EdgeOptions edgeOptions = new EdgeOptions();
                    edgeOptions.AddArgument("--disable-extensions");
                    edgeOptions.AddArgument("--start-maximized");
                  
                    if (options.Headless)
                    {
                        edgeOptions.AddArgument("--headless");
                    }
                    return new EdgeDriver(edgeOptions);
                }
                else
                {
                    FirefoxOptions firefoxOptions = new FirefoxOptions();
                    firefoxOptions.AddArgument("--disable-extensions");
                    firefoxOptions.AddArgument("--start-maximized");

                    if (options.Headless)
                    {
                        firefoxOptions.AddArgument("--headless");
                    }
                    return new FirefoxDriver(firefoxOptions);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error initializing WebDriver: {ex.Message}");
                return null;
            }
        }

        public static void KillChromeDriverProcesses()
        {
            foreach (var process in Process.GetProcessesByName("chromedriver"))
            {
                process.Kill();
            }
        }

    }
}




