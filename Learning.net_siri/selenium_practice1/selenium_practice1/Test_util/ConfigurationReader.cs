﻿using Microsoft.Extensions.Configuration;

namespace selenium_practice1.Test_util
{
    
public class ConfigurationReader

    {
        private IConfigurationRoot configuration;


        public ConfigurationReader()
        {
            string path = Path.Combine(Directory.GetCurrentDirectory()
                , "appsettings.json");
            if (!File.Exists(path))
            {
                throw new FileNotFoundException
                    ($"Configuration file not found: {path}");
            }


            var builder = new ConfigurationBuilder()
                .AddJsonFile(path, optional: true, reloadOnChange: true);


            configuration = builder.Build();
        }


        public WebDriverOptions GetWebDriverOptions()
        {
            var options = new WebDriverOptions();
            configuration.GetSection("WebDriverOptions").Bind(options);


            // Debugging output
            Console.WriteLine($"Configuration - Headless: {options.Headless}, Browser: {options.Browser}");


            return options;
        }

    }
}

