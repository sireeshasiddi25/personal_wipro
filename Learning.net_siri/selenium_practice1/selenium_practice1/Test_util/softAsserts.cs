﻿using NUnit.Framework.Legacy;
using NUnit.Framework;

namespace selenium_practice1.Test_util
{
    
public class SoftAssert

    {
        private List<string> _errors = new List<string>();


        public bool AreEqual(object expected, object actual, string message = "")
        {
            try
            {
                ClassicAssert.AreEqual(expected, actual, message);
                return true;
            }
            catch (AssertionException ex)
            {
                _errors.Add(ex.Message);
                return false;
            }
        }
        public bool AreNotEqual(object expected, object actual, string message = "")
        {
            try
            {
                ClassicAssert.AreNotEqual(expected, actual, message);
                return true;
            }
            catch (AssertionException ex)
            {
                _errors.Add(ex.Message);
                return false;
            }
        }


        public bool IsTrue(bool condition, string message = "")
        {
            try
            {
                ClassicAssert.IsTrue(condition, message);
                return true;
            }
            catch (AssertionException ex)
            {
                _errors.Add(ex.Message);
                return false;
            }
        }


        public void IsFalse(bool condition, string message = "")
        {
            try
            {
                ClassicAssert.IsFalse(condition, message);
            }
            catch (AssertionException ex)
            {
                _errors.Add(ex.Message);
            }
        }


        public void AssertAll()
        {
            if (_errors.Count > 0)
            {
                throw new AssertionException(string.Join("\n", _errors));
            }
        }

    }
}

