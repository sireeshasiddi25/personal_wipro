﻿using NUnit.Framework.Legacy;

namespace selenium_practice1.Test_util
{
    public  class HardAssert
    {
       
        public  void AreEqual(object expected, object actual, string message = "")
        {
            ClassicAssert.AreEqual(expected, actual, message);
        }


        public  void IsTrue(bool condition, string message = "")
        {
            ClassicAssert.IsTrue(condition, message);
        }


        public  void IsFalse(bool condition, string message = "")
        {
            ClassicAssert.IsFalse(condition, message);
        }

    }
}
