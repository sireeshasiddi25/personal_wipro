﻿using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using selenium_practice1.Test_util;

namespace selenium_practice1.Pages
{
     class magentoLoginPage
    {
        

        public IWebDriver driver;

        public WebDriverUtility wd;
        public WebDriverWait wait;
        public magentoLoginPage(IWebDriver driver)
        {
            this.driver = driver;
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10000));
            wd = new WebDriverUtility(driver);
            //wd.NavigateToPage("https://magento.softwaretestingboard.com/");
           // wd.SetPageLoadTimeout(10000);
        }
        public void NavigateToPage(string url)
        {
            driver.Navigate().GoToUrl(url);
        }
        public string signIn = "/html/body/div[2]/header/div[1]/div/ul/li[2]/a";
        public IWebElement signin => wd.FindElement(By.XPath(signIn));

        public string Email = "//*[@id=\"email\"]";
        public IWebElement email => wd.FindElement(By.XPath(Email));

        public string Password = "//*[@id=\"pass\"]";
        public IWebElement password => wd.FindElement(By.XPath(Password));
        public string signinbutton = "//*[@id=\"send2\"]/span";
        public IWebElement SignInButton => wd.FindElement(By.XPath(signinbutton));

        public string wrongEmail = "//*[@id=\"maincontent\"]/div[2]/div[2]/div/div/div";
        public IWebElement wrongemail => wd.FindElement(By.XPath(wrongEmail));

        public IWebElement createaccount => wd.FindElement(By.XPath("//*[@id=\"maincontent\"]/div[3]/div/div[2]/div[2]/div[2]/div/div/a/span"));
        public string enterFirstName = "//*[@id=\"firstname\"]";
        public string enterLastName = "//*[@id=\"lastname\"]";
        public string enterEmail = "//*[@id=\"email_address\"]";
        public string enterPassword = "//*[@id=\"password\"]";
        public IWebElement enterpassword => wd.FindElement(By.XPath(enterPassword));
        public string confirmPassword = "//*[@id=\"password-confirmation\"]";
        public IWebElement confirmpassword => wd.FindElement(By.XPath(confirmPassword));
        public string passwordError = "//*[@id=\"password-confirmation-error\"]";
        public IWebElement passworderror => wd.FindElement(By.XPath(passwordError));

        public string passwordStrong = "//*[@id=\"password-strength-meter-label\"]";
        public IWebElement passwordstrong=>wd.FindElement(By.XPath(passwordStrong));
        //*[@id="form-validate"]/div/div[1]/button/span
        public IWebElement createAnAccButton => wd.FindElement(By.XPath("//*[@id='form-validate']/div/div[1]/button/span"));


        public void EnterFirstName()
        { wd.SendKeysToElement(By.XPath(enterFirstName),"sireesha"); }

        public void EnterLastName()
        {
            wd.SendKeysToElement(By.XPath(enterLastName), "siddi");
        }
        public void EnterEmail()
        {
            wd.SendKeysToElement(By.XPath(enterEmail), "siddisirisha49@gmail.com");
        }

        public void EnterPasswordCreateAcc(string s)
        {
            wd.SendKeysToElement(By.XPath(enterPassword), s);
        }
        public void EnterConfirmPassword(string s)
        {
            wd.SendKeysToElement(By.XPath(confirmPassword), s);
        }
        public void EnterUserName(string s)
        {
            wd.SendKeysToElement(By.XPath(Email), s);
        }

        public void EnterPassword(string s)
        {
            wd.SendKeysToElement(By.XPath(Password), s);
        }

        public bool PasswordInputs()
        {
            Console.WriteLine("enter Password ");
            string s1 = Console.ReadLine();

            Console.WriteLine("enter cofirmed Password ");
            string p1 = Console.ReadLine();
            EnterConfirmPassword(p1);
            EnterPasswordCreateAcc(s1);

            if(s1==p1)
            {
                return true;
            }
            return false;
        }

        public void RemovePasswordConfirmPassword()
        {
            enterpassword.Click();
            wd.ClearText(By.XPath(enterPassword));
            confirmpassword.Click();
            wd.ClearText(By.XPath(confirmPassword));
        }
    }
}
