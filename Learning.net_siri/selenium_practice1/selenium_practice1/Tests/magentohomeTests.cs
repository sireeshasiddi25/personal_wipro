﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using selenium_practice1.Pages;
using selenium_practice1.Test_util;
using OpenQA.Selenium.Support.UI;
using NUnit.Framework.Legacy;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using Newtonsoft.Json;

namespace selenium_practice1.Tests
{
    public class UserCredentials
    {

        public string Username { get; set; }

        public string Password { get; set; }

    }
    [TestFixture]
      class magentohomeTests
    {
        private IWebDriver driver;
        private magentohomepage loginPage;

        public WebDriverUtility wd;
        public SoftAssert sa;
        public HardAssert ha;
        public WebDriverWait wait;

        /* [SetUp]
         public void TestSetup()
         {

             driver = new ChromeDriver();
             loginPage = new magentohomepage(driver);
             // cartPage.NavigateToPage("https://www.modere.co.in");
             wd = new WebDriverUtility(driver);
             wd.NavigateToPage("https://magento.softwaretestingboard.com/");
             sa = new SoftAssert();
              ha = new HardAssert();


             // wd.SetPageLoadTimeout(10000);

         }*/
        public ExtentReports extent;

        public ExtentTest test;

        [OneTimeSetUp]

        public void Setup()
        {

            // Initialize ExtentReports         

            var htmlReporter = new ExtentSparkReporter
                (@"C:\Reports\ExtentReport.html");

            extent = new ExtentReports();

            extent.AttachReporter(htmlReporter);



        }


        [SetUp]

        public void BeforeTest()
        {
            WebDriverUtility.KillChromeDriverProcesses();
            driver = new ChromeDriver();

            test = extent.CreateTest
                (TestContext.CurrentContext.Test.Name);

            loginPage = new magentohomepage(driver);
          
            wd = new WebDriverUtility(driver);
            wd.NavigateToPage("https://magento.softwaretestingboard.com/");

        }


        [TearDown]

        public void AfterTest()
        {

            // Close the browser        

            driver.Quit();

            // End the test and add it to the report        

            extent.Flush();

        }


        [OneTimeTearDown]

        public void TearDown()
        {

            // Close the ExtentReports instance         

            extent.Flush();

        }

        public static IEnumerable<TestCaseData> GetTestData()
        {
            var currentDir = Directory.GetCurrentDirectory();

            var jsonFilePath = Path.Combine(currentDir,
                "TestData\\credentials1.json");

            var jsonData = File.ReadAllText(jsonFilePath);

            var credentials = JsonConvert.DeserializeObject<List<UserCredentials>>(jsonData);

            foreach (var credential in credentials)
            {
                Console.WriteLine($"Loaded credential: {credential.Username}, {credential.Password}");
                yield return new TestCaseData(credential.Username, credential.Password);

            }

        }

        [Test, TestCaseSource("GetTestData")]
        public void clickItems(string Username, string Password)
        {
            loginPage.signin.Click();
            test.Log(Status.Pass, "signin click passed");
            loginPage.EnterUserName(Username);
            test.Log(Status.Pass, "enterusername passed");
            loginPage.EnterPassword(Password);
            test.Log(Status.Pass, "enter password passed");
            loginPage.SignInButton.Click();
            test.Log(Status.Pass, "signinbutton click passed");


            loginPage.Product.Click();
            test.Log(Status.Pass, "product click passed");
            loginPage.addelement.Click();
            test.Log(Status.Pass, "add element click passed");


            wd.setImplicitWait(TimeSpan.FromSeconds(20000));
            test.Log(Status.Pass, "implicit wait passed");
            ClassicAssert.IsTrue(loginPage.addproductmsg.Displayed, "no");
            test.Log(Status.Pass, " addproductmsg displayed passed");
            loginPage.homefrombag.Click();
            test.Log(Status.Pass, "homefrombag click passed");
            loginPage.scrollToProduct1();
            test.Log(Status.Pass, "scrolltoproduct passed");
            loginPage.mouseHover();
            test.Log(Status.Pass, "mouseHover passed");
            loginPage.product1color.Click();
            test.Log(Status.Pass, "product1color passed");
            loginPage.product1size.Click();
            test.Log(Status.Pass, "product1size passed");
            loginPage.mouseHover();
            test.Log(Status.Pass, "mousehover on product1 passed");
            loginPage.addproduct1.Click();
            test.Log(Status.Pass, " add product1 passed");

            //wd.setImplicitWait(TimeSpan.FromSeconds(20000));
            loginPage.FluentWait();
            test.Log(Status.Pass, "fluentwait passed");

            // ClassicAssert.IsTrue(loginPage.product1addmsg.Displayed, "no");
            ClassicAssert.IsTrue(loginPage.product1addmsg.Displayed, "no");
            test.Log(Status.Pass, "  product1add msg displayed passed");

            //ha.IsFalse(loginPage.product1addmsg.Displayed, "no");

            loginPage.clickcart.Click();
            test.Log(Status.Pass, "clickcart  passed");
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10000));
            test.Log(Status.Pass, "wait passed");

            loginPage.editbagdetails.Click();
            test.Log(Status.Pass, "edibagdetails passed");
            loginPage.bagquantity.Click();
            test.Log(Status.Pass, "bagquantity click passed");
            loginPage.clearQuantityText();
            test.Log(Status.Pass, "clearQuantity text passed");
            loginPage.typeQuantityText("2");
            test.Log(Status.Pass, "typeQuantityText passed");
            loginPage.GetTextMethod();
            test.Log(Status.Pass, "getTextMethod passed");

            loginPage.updatebagcartbutton.Click();
            //loginPage.productquantity.Click();

            loginPage.proceedtocart.Click();
            test.Log(Status.Pass, "proceedtocart passed");
            test.Log(Status.Pass, "ClickItems passed");
            /*  }
              catch(Exception ex)
              {
                  test.Log(Status.Fail,"Assertion failed"+ex.Message);
                  throw;

                  //Console.WriteLine
              }*/

        }
        [Test]
        public void FunctionalityCheck()
        {
            loginPage.getAttributeValeForWomen();
            test.Log(Status.Pass, "getAttribueValuefor women passed");

        }
        [Test]
        public void PageFunctionalitiesCheck()
        {
            loginPage.pageFunctions();
            test.Log(Status.Pass, " PageFunctionalitiesCheck passed");
        }
        [Test]
        public void AlertFunctionsCheck()
        {
            loginPage.AlertFunctions();
            test.Log(Status.Pass, " AlertFunctionscheck passed");
        }
        [Test]
        public void DragAndDropCheck()
        {

            loginPage.signin.Click();
            test.Log(Status.Pass, "signin click passed");
            loginPage.EnterUserName("sireeshasiddi25@gmail.com");
            test.Log(Status.Pass, "enterusername passed");
            loginPage.EnterPassword("Sirisha.nana25");
            test.Log(Status.Pass, "enter password passed");
            loginPage.DragAndDropFunction();
            test.Log(Status.Pass, " DragAndDropCheck passed");
        }
        [Test]
        public void OrderTableData()
        {
            loginPage.scrollToProduct1();
            test.Log(Status.Pass, "scrolltoproduct passed");
            loginPage.mouseHover();
            test.Log(Status.Pass, "mouseHover passed");
            loginPage.product1color.Click();
            test.Log(Status.Pass, "product1color passed");
            loginPage.product1size.Click();
            test.Log(Status.Pass, "product1size passed");
            loginPage.mouseHover();
            test.Log(Status.Pass, "mousehover on product1 passed");
            loginPage.addproduct1.Click();
            test.Log(Status.Pass, " add product1 passed");

            
            loginPage.FluentWait();
            test.Log(Status.Pass, "fluentwait passed");

            // ClassicAssert.IsTrue(loginPage.product1addmsg.Displayed, "no");
            ClassicAssert.IsTrue(loginPage.product1addmsg.Displayed, "no");
            test.Log(Status.Pass, "  product1add msg displayed passed");

            //ha.IsFalse(loginPage.product1addmsg.Displayed, "no");

            loginPage.clickcart.Click();
            test.Log(Status.Pass, "clickcart  passed");
            
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10000));
            test.Log(Status.Pass, "wait passed");

            loginPage.viewandeditcartMethod();
            test.Log(Status.Pass, "viewandeditcart passed");
            loginPage.OrderTableFunctions();
            test.Log(Status.Pass, "OrderTableFunctions passed");

        }
        [Test]
        public void DropDownFunction()
        {
            loginPage.DropDownWomenHome();
        }

        /* [TearDown]
         public void TearDown()
         {
             driver.Quit();
         }*/
    }
}
