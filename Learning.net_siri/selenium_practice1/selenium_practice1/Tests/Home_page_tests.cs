﻿using NUnit.Framework.Legacy;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using selenium_practice1.Pages;
using selenium_practice1.Test_util;


namespace selenium_practice1.Tests
{
    [TestFixture]
    public class Home_page_tests
    {
        private IWebDriver driver;
        private HomePage homePage;

        public WebDriverUtility wdu;

        [SetUp]
        public void TestSetup()
        {
           
            driver = new ChromeDriver();
            homePage = new HomePage(driver);
            //homePage.NavigateToPage("https://www.Modere.com/");
            wdu = new WebDriverUtility(driver);
            wdu.NavigateToPage("https://www.Modere.com/");
          
        }

       /* [Test]
        public void verifyWelcomeTextOnHomePage()
        {
           // wdu.setImplicitWait(TimeSpan.FromSeconds(200));
            //homePage.NavigateToPage("https://www.Modere.com");
            ClassicAssert.IsTrue(homePage.webElement.Displayed,
                "Element is not Displayed");
        }*/

        [Test]
        public void verifyModereLogoimg()
        {
            ClassicAssert.IsTrue(homePage.webElement1.Displayed,
                   "Element is not Displayed");
        }

        [Test]
        public void verifyModereReward()
        {
            ClassicAssert.IsNotNull(homePage.webElement2,
                   "Element is not Displayed");
        }
        [Test]
        public void verifyModereReward2()
        {
            //ClassicAssert.IsTrue(homePage.webElement2.Enabled,
                //   "Element is not Displayed");
                bool isClickable= homePage.webElement2.Displayed;
            ClassicAssert.IsFalse(isClickable,
                   "Element is not Displayed");
        }

        [Test]
        public void verifyModereShopNow3()
        {
            //ClassicAssert.IsTrue(homePage.webElement2.Enabled,
            //   "Element is not Displayed");
            bool isClickable = homePage.webElement3.Displayed;
            ClassicAssert.IsTrue(isClickable, "element is not  clickable");
        }

        [Test]
        public void verifyModereshop()
        {
            ClassicAssert.IsTrue(homePage.webElement3.Enabled,
                   "Element is not Displayed");

        }

        [Test]
        public void verifyModerevideo()
        {
            ClassicAssert.IsTrue(homePage.webElement4.Displayed,
                   "Element is not Displayed");
        }
        [Test]
        public void verifyModereBlog()
        {
            ClassicAssert.IsNotNull(homePage.webElement5,
                   "Element is not Displayed");
        }

        [Test]
        public void verifyModereProfile()
        {
            //ClassicAssert.IsFalse(homePage.webElement6.Displayed,
            //      "Element is not Displayed");

            ClassicAssert.IsTrue(homePage.webElement6.Enabled,
                   "Element is not Enabled");

        }

        [Test]
        public void verifyModereshopNow2()
        {
            // ClassicAssert.IsNotNull(homePage.clickOnShop(driver));
            for (int i = 0; i < homePage.listOfMainMenuItems.Count; i++)
            {             
                Console.WriteLine(homePage.listOfMainMenuItems[i].Enabled);
            }
          //  ClassicAssert.IsTrue(homePage.listOfMainMenuItems[0].Displayed,
              //     "Element is not Displayed");
        }
        [Test]
        public void VerifyAllListElementsDisplayed()
        {
            ClassicAssert.IsTrue(homePage.AreAllListElementsDisplayed(), 
                "Not all list elements are displayed.");
        }
       
        [Test]
        public void ClickOnShopTest1()
        {
             bool isClickable = homePage.webElement3.Displayed
                && homePage.webElement3.Enabled;
     
            ClassicAssert.IsTrue(homePage.clickOnShop
                (isClickable),
                "Not all list elements are displayed.");
        }
        [Test]
        public void setWait1()
        {
            
            TimeSpan waitTime=TimeSpan.FromSeconds(100);
            homePage.setWait(waitTime);
            Console.WriteLine("I'm setWait1 test");


        }
        [Test]
       public void waitForAnElementHomePagetTests()
        {
            TimeSpan waitTime = TimeSpan.FromSeconds(10);
            homePage.waitForAnElementHomePage(waitTime);
            Console.WriteLine("I'm waitForAnElementHomePagetTests test");
        }

        [Test]
        public void FluentWaitForElementHomePageTests()
        {

            TimeSpan waitTime = TimeSpan.FromSeconds(20);
            TimeSpan pollTime = TimeSpan.FromSeconds(5);
            homePage.FluentWaitForElementHomePage(waitTime,pollTime);
            Console.WriteLine("I'm FluentWaitForElementHomePageTests test");

        }
        [Test]
        public void DoubleClickElementHomePageTests()
        {
            homePage.DoubleClickElementHomePage();
            Console.WriteLine("I'm DoubleClickElementHomePageTests test");
        }

        [Test]
        public void ScrollToElementHomePageTests()
        {
            homePage.ScrollToElementHomePage();
            Console.WriteLine("I'm ScrollToElementHomePageTests test");
        }

        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }
    }
}



