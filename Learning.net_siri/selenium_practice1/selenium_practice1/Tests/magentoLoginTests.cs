﻿using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using selenium_practice1.Pages;
using selenium_practice1.Test_util;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;


namespace selenium_practice1.Tests
{

    [TestFixture]
     class magentoLoginTests
    {
        private IWebDriver driver;
        private magentoLoginPage loginPage;

        public WebDriverUtility wd;
        public WebDriverWait wait;
        public SoftAssert sa;
        public HardAssert ha;
        public WebDriverOptions wdo;

        /* [SetUp]
         public void TestSetup()
         {
             WebDriverUtility.KillChromeDriverProcesses();
             // Read WebDriver options from configuration

             var configReader = new ConfigurationReader();

             wdo = configReader.GetWebDriverOptions();

             // Initialize WebDriver based on configuration   

             driver = WebDriverUtility.InitializeDriver(wdo);


             //driver = new ChromeDriver();
             loginPage = new magentoLoginPage(driver);
             // cartPage.NavigateToPage("https://www.modere.co.in");
             wd = new WebDriverUtility(driver);
             wd.NavigateToPage("https://magento.softwaretestingboard.com/");

             // wd.SetPageLoadTimeout(10000);
              sa=new SoftAssert();
              ha=new HardAssert();



         }*/

        public ExtentReports extent;

        public ExtentTest test;

        [OneTimeSetUp]

        public void Setup()
        {
            WebDriverUtility.KillChromeDriverProcesses();
            // Read WebDriver options from configuration

            var configReader = new ConfigurationReader();

            wdo = configReader.GetWebDriverOptions();

            

            // Initialize ExtentReports         

            var htmlReporter = new ExtentSparkReporter
                (@"C:\Reports\ExtentReport.html");

            extent = new ExtentReports();

            extent.AttachReporter(htmlReporter);

           

        }


        [SetUp]

        public void BeforeTest()
        {
            // Initialize WebDriver based on configuration   

            driver = WebDriverUtility.InitializeDriver(wdo);
            //  driver = new ChromeDriver();

            test = extent.CreateTest
                (TestContext.CurrentContext.Test.Name);

            loginPage = new magentoLoginPage(driver);
            // cartPage.NavigateToPage("https://www.modere.co.in");
            wd = new WebDriverUtility(driver);
            wd.NavigateToPage("https://magento.softwaretestingboard.com/");

        }


        [TearDown]

        public void AfterTest()
        {

            // Close the browser        

            driver.Quit();

            // End the test and add it to the report        

            extent.Flush();

        }


        [OneTimeTearDown]

        public void TearDown()
        {

            // Close the ExtentReports instance         

            extent.Flush();

        }

       [Test]
        public void SignInItems()
        {
            loginPage.signin.Click();

            loginPage.EnterUserName("sireeshasiddi25@gmail.com");
            // sa.AreEqual("", loginPage.wrongemail, "enter correct email");
            
            loginPage.EnterPassword("Sirisha.nana25");
            loginPage.SignInButton.Click();
           // if (sa.AreEqual("https://magento.softwaretestingboard.com/customer/account/login/referer/aHR0cHM6Ly9tYWdlbnRvLnNvZnR3YXJldGVzdGluZ2JvYXJkLmNvbS9jdXN0b21lci9hY2NvdW50L2xvZ291dFN1Y2Nlc3Mv/"
             //   , "https://magento.softwaretestingboard.com/customer/account/", "check email or password"))

                if ("https://magento.softwaretestingboard.com/customer/account/login/referer/aHR0cHM6Ly9tYWdlbnRvLnNvZnR3YXJldGVzdGluZ2JvYXJkLmNvbS9jdXN0b21lci9hY2NvdW50L2xvZ291dFN1Y2Nlc3Mv/"
                !="https://magento.softwaretestingboard.com/customer/account/")
                {
                loginPage.createaccount.Click();
                loginPage.EnterFirstName();
                loginPage.EnterLastName();
                loginPage.EnterEmail();
                //loginPage.PasswordInputs();
                loginPage.EnterPasswordCreateAcc("Siri.nana25");
                loginPage.EnterConfirmPassword("Siri.nana25");

                loginPage.createAnAccButton.Click();


               /* if (sa.IsTrue((loginPage.passworderror.Enabled),"false"))
                {
                    loginPage.RemovePasswordConfirmPassword();
                    loginPage.EnterPasswordCreateAcc("Siri.nana25");
                    loginPage.EnterConfirmPassword("Siri.nana25");
                }*/
                }




        }
       /* [TearDown]
        public void TearDown()
        {
            driver.Quit();

        }*/
    }
}
