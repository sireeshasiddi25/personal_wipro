﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice5
{
     class creation_of_operations
    {
        static void main()
        {
            var num = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var a = num.ToObservable();
            var b = Observable.Range(0, 5);
            var timer = Observable.Timer(TimeSpan.FromSeconds(1));
            a.Subscribe(Console.WriteLine);
            b.Subscribe(Console.WriteLine);
            timer.Subscribe(_ =>Console.WriteLine("time completed "));
            Console.ReadLine();
        }

    }
}
