﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice5
{
     class transformation_of_operators
    {
        static void main(string[] args)
        {
            var num = Observable.Range(0, 10);


            var transformNum = num.Select(x => x * 10);


            transformNum.Subscribe(Console.WriteLine);


        }
    }
}
