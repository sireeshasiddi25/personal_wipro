﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice5
{
    public delegate int Delegate_tables(int num);
    class delegate_table
    {
        public int Tables(int number)
        {
            for(int i=1;i<=10;i++)
            { 
                Console.WriteLine(number+"*"+i+"= " + (number*i)); 
            }
            return number;
        }

        public static void Main(string[] args)
        {
            delegate_table d = new delegate_table();
            Delegate_tables del = d.Tables;

            Console.WriteLine("Enter number of tables u wanted to print");
            int a = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < a; i++)
            {
                Console.WriteLine("enter number to calculate table");
                int b=Convert.ToInt32(Console.ReadLine());
                int n = del(b);
                Console.WriteLine("For number " + n+"\n");
            }
        }
    }
}
