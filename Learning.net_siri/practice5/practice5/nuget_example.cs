﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Linq;

namespace practice5
{
     class nuget_example
    {
        static void main()
        {
            var observable = Observable.Range(0, 5);
            List<string> list = new List<string>();
            list.Add("siri");
            list.Add("Rupa");
            list.Add("suma");
            list.Add("Gouthu");
            list.Add("Ram");

            IDisposable subscription= observable.Subscribe(

            onNext: value => Console.WriteLine($"Received:{value}"),
            onError: ex=>Console.WriteLine($"Error:{ex.Message}"),
            onCompleted: ()=> Console.WriteLine("Sequence Completed\n")

            );

            IDisposable subscription1 = observable.Subscribe(
            onNext: value => Console.WriteLine($"Received:{list[value]}"),
            onError: ex => Console.WriteLine($"Error:{ex.Message}"),
            onCompleted: () => Console.WriteLine("Sequence1 Completed\n"));

            Thread.Sleep(5000);
            subscription.Dispose();
            subscription1.Dispose();

        }
    }
}
