﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace practice5
{
     class Employee_salary_department
    {
        static List<Employee2> GetEmployeeData()
        {

            return new List<Employee2>
            {
                new Employee2 {Id = 101, Name = "Sharan", Department = "IT",Salary=2500000},
                new Employee2 {Id = 102, Name = "Sireesha", Department = "IT",Salary=1000000},
                new Employee2 {Id = 103, Name = "Somu", Department = "IT",Salary=990000},
                new Employee2 {Id = 104, Name = "Amarnath", Department = "IT",Salary=2000000},
                new Employee2 {Id = 105, Name = "Sanskar", Department = "Management",Salary=900000},
                new Employee2 {Id = 106, Name = "Trisha", Department = "HR",Salary=950000},
                new Employee2 {Id = 107, Name = "Gokul", Department = "Fin",Salary=2500000},
                new Employee2 {Id = 108, Name = "Gautham", Department = "Management",Salary=5000000}
            };
        }


        static void AddEmployee(Subject<Employee2> subject, Employee2 employee)
        {
            
                subject.OnNext(employee);

             
        }
        static void main(string[] args)
        {

            var observableEmployees = GetEmployeeData().ToObservable();
            

            var a = from employee in observableEmployees
                    where employee.Department == "IT" 
                    && employee.Salary >= 1000000
                    select employee;

           

            var employeeSubject = new Subject<Employee2>();

            IDisposable subscription = a.Subscribe(

             onNext: employee => Console.WriteLine($"New employee added: {employee}"),
             onError: ex => Console.WriteLine($"Error: {ex.Message}"),
             onCompleted: () => Console.WriteLine("Employee data stream completed.")
             );


            IDisposable subscription1 = employeeSubject.
                Where(employee => employee.Department == "IT"&&
                employee.Salary>=1000000).Subscribe(

             onNext: employee => Console.WriteLine($"New employee added: {employee}"),
             onError: ex => Console.WriteLine($"Error: {ex.Message}"),
             onCompleted: () => Console.WriteLine("regular Employee data " +
             "stream completed.")
             );


            AddEmployee(employeeSubject, new Employee2
            { Id = 109, Name = "Anil", Department = "IT", Salary=2000000 });
            AddEmployee(employeeSubject, new Employee2
            { Id = 110, Name = "Ajay", Department = "Management",Salary=3000000 });


            Thread.Sleep(5000);
            subscription.Dispose();

            subscription1.Dispose();
        }
    }


    public class Employee2
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Department { get; set; }
        public double  Salary { get; set; }


        public override string ToString()
        {
            return $"Employee ID: {Id}, Name: {Name}, Department: {Department}," +
                $"Salary : {Salary}";
        }
    }
}



