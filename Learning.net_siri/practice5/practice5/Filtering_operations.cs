﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice5
{
     class Filtering_operations
    {

        static void main(string[] args)
        {
            var num1 = Observable.Range(1, 5);

            Console.WriteLine("even numbers");
            var evenNum1 = num1.Where(x => x % 2 == 0);
            evenNum1.Subscribe(Console.WriteLine);

            Console.WriteLine("Sum");

            var num = num1.Sum();
            num.Subscribe(Console.WriteLine);

            
        }
    }
}
