﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice5
{
    class Example1
    {
        static List<Employee1> GetEmployeeData()
        {

            return new List<Employee1>
            {
                new Employee1 {Id = 101, Name = "Sharan", Department = "IT"},
                new Employee1 {Id = 102, Name = "Sireesha", Department = "IT"},
                new Employee1 {Id = 103, Name = "Somu", Department = "IT"},
                new Employee1 {Id = 104, Name = "Amarnath", Department = "IT"},
                new Employee1 {Id = 105, Name = "Sanskar", Department = "Management"},
                new Employee1 {Id = 106, Name = "Trisha", Department = "HR"},
                new Employee1 {Id = 107, Name = "Gokul", Department = "Fin"},
                new Employee1 {Id = 108, Name = "Gautham", Department = "Management"}
            };
        }


        static void AddEmployee(Subject<Employee1> subject, Employee1 employee)
        {
            ////if (employee.Department == "Management")
            {
                subject.OnNext(employee);
               
                //Console.WriteLine($"Employee added: {employee}");

            }
        }
            static void main(string[] args)
            {

                var observableEmployees = GetEmployeeData().ToObservable();
                // .Where(Employee => Employee.Department == "Management");

                var a = from employee in observableEmployees
                        where employee.Department == "Management"
                        select employee;

                // var emp = observableEmployees.
                //Where(Employee=>Employee.Department == "Management");

                var employeeSubject = new Subject<Employee1>();

                IDisposable subscription = a.Subscribe(

                 onNext: employee => Console.WriteLine($"New employee added: {employee}"),
                 onError: ex => Console.WriteLine($"Error: {ex.Message}"),
                 onCompleted: () => Console.WriteLine("Employee data stream completed.")
                 );


                IDisposable subscription1 = employeeSubject.
                    Where(employee => employee.Department == "Management").Subscribe(

                 onNext: employee => Console.WriteLine($"New employee added: {employee}"),
                 onError: ex => Console.WriteLine($"Error: {ex.Message}"),
                 onCompleted: () => Console.WriteLine("regular Employee data " +
                 "stream completed.")
                 );


                AddEmployee(employeeSubject, new Employee1
                { Id = 109, Name = "Anil", Department = "Trainer" });
                AddEmployee(employeeSubject, new Employee1
                { Id = 110, Name = "Ajay", Department = "Management" });


                Thread.Sleep(5000);
                subscription.Dispose();
            
                subscription1.Dispose();
            }
    }


        public class Employee1
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Department { get; set; }


            public override string ToString()
            {
                return $"Employee ID: {Id}, Name: {Name}, Department: {Department}";
            }
        }
}




