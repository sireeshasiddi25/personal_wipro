﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice5
{
     class Highest_salary_employee
    {
        static List<Employee2> GetEmployeeData()
        {

            return new List<Employee2>
            {
                new Employee2 {Id = 101, Name = "Sharan", Department = "IT",Salary=2500000},
                new Employee2 {Id = 102, Name = "Sireesha", Department = "IT",Salary=1000000},
                new Employee2 {Id = 103, Name = "Somu", Department = "IT",Salary=990000},
                new Employee2 {Id = 104, Name = "Amarnath", Department = "IT",Salary=2000000},
                new Employee2 {Id = 105, Name = "Sanskar", Department = "Management",Salary=900000},
                new Employee2 {Id = 106, Name = "Trisha", Department = "HR",Salary=950000},
                new Employee2 {Id = 107, Name = "Gokul", Department = "Fin",Salary=2500000},
                new Employee2 {Id = 108, Name = "Gautham", Department = "Management",Salary=5000000}
            };
        }
        static void main()
        {
            var data = GetEmployeeData().ToObservable();

            Console.WriteLine("employee with highest salary");
            var highest = data.Max(e=>e.Salary);
            highest.Subscribe(Console.WriteLine);

            Console.WriteLine("employee with lowest salary");
            var lowest = data.Min(e => e.Salary);
            lowest.Subscribe(Console.WriteLine);


            Console.WriteLine("Average salary of employees");
            var average = data.Average(e => e.Salary);
            average.Subscribe(Console.WriteLine);

            Console.WriteLine("Count of employees");
            var count = data.Count();
            count.Subscribe(Console.WriteLine);

            Console.WriteLine("sum of employee salaries");
            var sum = data.Sum(e => e.Salary);
            sum.Subscribe(Console.WriteLine);
        }
    }
}
