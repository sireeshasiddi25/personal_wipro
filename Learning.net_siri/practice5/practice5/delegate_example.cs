﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice5
{
    public delegate int MyDelegate(int num1, int num2);
     class delegate_example
    {

            public int Method1(int number1, int number2)
            {
                Console.WriteLine("Method1: " + number1);
                Console.WriteLine("Method1: " + number2);
                Console.WriteLine("Sum of Numbers: " + (number1 + number2));
                return number1 + number2;
            }


            public static void main(string[] args)
            {
                delegate_example d = new delegate_example();
                MyDelegate del = d.Method1;


                int sum = del(5, 10);
                Console.WriteLine("Sum: " + sum);
            }


        }
    }
