﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace practice5
{
    class Example
    {
        static List<Employee> GetEmployeeData()
        {
           
                return new List<Employee>
            {
             
                new Employee {Id = 101, Name = "Sharan", Department = "IT"},
                new Employee {Id = 102, Name = "Sireesha", Department = "IT"},
                new Employee {Id = 103, Name = "Somu", Department = "IT"},
                new Employee {Id = 104, Name = "Amarnath", Department = "IT"},
                new Employee {Id = 105, Name = "Sanskar", Department = "Management"},
                new Employee {Id = 106, Name = "Trisha", Department = "HR"},
                new Employee {Id = 107, Name = "Gokul", Department = "Fin"},
                new Employee {Id = 108, Name = "Gautham", Department = "Management"},


            };
        }


        static void AddEmployee(Subject<Employee> subject, Employee employee)
        {
           
                subject.OnNext(employee);
                Console.WriteLine($"Employee added: {employee}");
            
        }
        static void main(string[] args)
        {

            var observableEmployees = GetEmployeeData().ToObservable();
               
            var employeeSubject = new Subject<Employee>();
            
                IDisposable subscription = observableEmployees.Subscribe(
                
                onNext: employee => Console.WriteLine($"New employee added: {employee}"),
                 onError: ex => Console.WriteLine($"Error: {ex.Message}"),
                 onCompleted: () => Console.WriteLine("Employee data stream completed.")
                 );


            AddEmployee(employeeSubject, new Employee 
            { Id = 109, Name = "Anil", Department = "Trainer" });
            AddEmployee(employeeSubject, new Employee 
            { Id = 109, Name = "Ajay", Department = "Management" });


            Thread.Sleep(5000);
            subscription.Dispose();
        }
    }


    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Department { get; set; }


        public override string ToString()
        {
            return $"Employee ID: {Id}, Name: {Name}, Department: {Department}";
        }
    }
}

