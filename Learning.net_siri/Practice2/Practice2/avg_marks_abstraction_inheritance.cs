﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice2
{
     abstract class avg_marks_abstraction_inheritance
    {
        public abstract double avg(int[] a);
        public abstract void print();

    }
    class student :avg_marks_abstraction_inheritance
    {
        
        public override double avg(int[] a)
        {
            int sum = 0;
            foreach(int i in a)
            {
                sum=sum+i;
            }
            double average=sum/a.Length;
            return average;
        }
        public override void print()
        {
            Console.WriteLine("Thank you !");
        }

    }
    class main7
    {
        public static void main()
        {
            Console.WriteLine("enter no of students you wanted to check average ");
            int n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("enter marks of a student for "+n+" subjects" +
                " in different lines ");
            int[] a=new int[n];
            for (int i=0;i<n;i++)
            {
                a[i]= Convert.ToInt32(Console.ReadLine());
            }

            student s = new student();
            Console.WriteLine("average marks is : "+s.avg(a));
            s.print();
        }
    }
}
