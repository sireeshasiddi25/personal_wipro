﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice2
{
    internal class BaseClass
    {
        public int i;
        public BaseClass(int i) 
        {
            this.i=i;
            Console.WriteLine("Base class - constructor i value " + i);
            Console.WriteLine("this.i" + this.i);
        }
        public BaseClass() { this.i = 5; }
        public int returnField() 
        {
            return this.i;
        }
        public static int returnValue()
        {
            return 20;
        }


    }
    class ChildClass : BaseClass
    {
        public int returnValue(int i)
        {
            return 40;
        }
        public int i = BaseClass.returnValue();

    }
    public class DrivenClass
    {
        public static void main(string[] args)
        {
            BaseClass obj = new BaseClass(100);
            Console.WriteLine(obj.ToString());
            int j = obj.returnField();
            Console.WriteLine("value of j :"+j);
            ChildClass obj1 = new ChildClass();
            int k = obj1.returnField();
            Console.WriteLine("print k " + k);
            //int i = obj1.i;
            Console.WriteLine("Print i " + obj1.i);
        }
    }
}
