﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice2
{
     class protected_member_inherited_class
    {
        protected int num=10;
        protected string s = "siri";
        protected void print()
        {
            Console.WriteLine("number " + num + " name " + s);
        }

    }
    class inherit :protected_member_inherited_class
    {
        public void print()
        {
            base.print();
            Console.WriteLine("Im in child class");
        }
        public void output()
        {
            Console.WriteLine("number " + num + " name " + s);
        }
       
    }
    class main6
    {
        public static void main()
        {
            inherit i = new inherit();
            i.print();
            i.output();
        }
    }
}
