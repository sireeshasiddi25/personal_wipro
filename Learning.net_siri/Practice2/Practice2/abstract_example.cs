﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice2
{
    abstract class Car
    {
        public abstract int doors();
        public  void print()
        {
            Console.WriteLine("this is absract class");
        }
    }
    class ChildCar : Car
    {
        public override int doors()
        {
            Console.WriteLine("override doors method");
            return 5;
        }
      

    }
    class Main3
    {
        public static void main(string[] args)
        {

            ChildCar c=new ChildCar();
            Console.WriteLine("child car doors "+c.doors());
            c.print();
        }
    }
}
