﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice2
{
     class reverse_string
    {
        public static void main()
        {
            Console.WriteLine("enter your string");
            string s = Console.ReadLine();
            Console.WriteLine("reverse of a string is ");
            for(int i=s.Length-1;i>=0;i--)
            {
                Console.Write(s[i]);
            }
            Console.WriteLine();
            char[] s2=new char[s.Length];
            int j = 0;
            for(int i=s.Length-1; i>=0;i--)
            {
                s2[j] = s[i];
                j++;

            }
            Console.WriteLine("Using array to store characters");
            foreach(char c in s2)
            {
                Console.Write(c);
            }

        }
    }
}
