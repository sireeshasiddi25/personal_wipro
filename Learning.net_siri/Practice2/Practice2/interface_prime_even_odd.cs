﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Practice2
{
    interface define
    {
        public void prime(int n);
        public void Result(bool a,int n);
    }
     class interface_prime_even_odd : define
    {

        public void prime(int num)
        {
            bool r = true;

            if (num <= 1)
            {
                r = false;
            }
            for (int i = 2; i <= num / 2; i++)
            {
                
                 if (num % i == 0)
                {
                    r = true;
                    break;
                }
                else
                {
                    r = false;
                }
            }
            
            Result(r,num);
        }
        public void Result(bool r,int num)
        { 

                if (r)
                {
                    if (num % 2 == 0)
                    {
                        Console.WriteLine("your number " + num + " is even and prime");
                    }
                    else
                    {
                        Console.WriteLine("your number " + num + " is odd and prime");
                    }
                }
                else
                {
                    if (num % 2 == 0)
                    {
                        Console.WriteLine("your number " + num + " is even and non prime");
                    }
                    else
                    {
                        Console.WriteLine("your number " + num + " is odd and non prime");
                    }
                }

            }
        public static void main()
        {
            Console.WriteLine("enter no of numbers u wanted to check the functionality");
            int num = Convert.ToInt32(Console.ReadLine());
            int[] m=new int[num];
            Console.WriteLine("enter " + num + " numbers in different lines");
            for(int x=0;x<num;x++)
            {
                m[x]=Convert.ToInt32(Console.ReadLine());
            }
            interface_prime_even_odd i = new interface_prime_even_odd();
            foreach(int x in m)
            {
                i.prime(x);
            }
            
        }
     }
}
