﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice2
{
    public class Cal3
    {
        public virtual void add()
        {
            int a = 20;
            int b = 40;
            Console.WriteLine("sum in void add " + a + b);

        }
        public virtual int add(int a, int b)
        {
            return a + b;
        }
        public virtual int add(int a, int b, int c)
        {
            return a + b + c;
        }
        public virtual double add(double a, double b)
        {
            return a + b;
        }
    }
    public class Cal2 : Cal3
    {
        public override void add()
        {
            base.add();
            Console.WriteLine("child class void add");

        }
        public override int add(int a, int b)
        {
            int sum = base.add(4, 5);
            sum = sum + a + b;
            return sum;
        }
        public override int add(int a, int b, int c)
        {
            return a + b + c;
        }
        public override double add(double a, double b)
        {
            return a + b;
        }


    }
    public class Main2
    {
        public static void main(string[] args)
        {
            Cal2 cal = new Cal2();
            cal.add();
            Console.WriteLine("2 int params " + cal.add(78, 96));
            Console.WriteLine("3 int params " + cal.add(78, 96, 56));
            Console.WriteLine("2 double params " + cal.add(78.89, 96.65));



        }
    }
}
