﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice2
{
    internal class Cal
    {
        public void add()
        {
            int a = 20;
            int b = 40;
            Console.WriteLine("sum in void add "+ a + b);

        }
        public int add(int a,int b)
        {
            return a + b;
        }
        public int add(int a,int b,int c)
        {
            return a + b + c;
        }
        public double add(double a,double b) 
        { 
            return a + b;
        }
    }
    public class main1
    {
        public static void main(string[] args)
        {
            Cal cal = new Cal();
            cal.add();
            Console.WriteLine("2 int params " + cal.add(78, 96));
            Console.WriteLine("3 int params " + cal.add(78, 96,56));
            Console.WriteLine("2 double params " + cal.add(78.89, 96.65));



        }
    }
}
