﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice2
{
     class example1
    {
        public static void print()
        {
            Console.WriteLine("I'm static method");
        }

        public void output()
        {
            Console.WriteLine("I'm non static method");
        }

    }
    class main8
    {
        static void ex()
        {
            example1.print();
        }
        public static void main()
        {
            example1 e = new example1();
            e.output();
            main8.ex();
        }
    }
}
