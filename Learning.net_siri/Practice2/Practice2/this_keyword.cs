﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice2
{
    class this_keyword
    {
        public int id = 10;
        public string name = "siri";
        public void instantiate(int id,string name)
        {
            Console.WriteLine("name " + name);
            Console.WriteLine("id " + id);
            id=this.id;
            name=this.name ;
            Console.WriteLine("this.name " + name);
            Console.WriteLine("this.id " +id);
        }
    }
    public class Child
    {
        public static void main(string[] args)
        {
            this_keyword t=new this_keyword();
            t.instantiate(15, "rupa");
            t.instantiate(20, "ram");
        }
    }
}
