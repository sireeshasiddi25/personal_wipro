﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice2
{
    class Bubble_sort
    {
        
        public static void main(string[] args)
        {
            Console.WriteLine("enter size of the array");
            int size=Convert.ToInt32(Console.ReadLine());

            int[] arr = new int[size];
            for (int i = 0; i < size; i++)
            {
                Console.WriteLine("enter value in an array for index" + i);
                arr[i] = Convert.ToInt32(Console.ReadLine());
            }
            //int[] arr2=new int[size];
            int temp = 0;
            for (int i=0; i < size; i++) 
            {
                for (int j = 0; j < size - i - 1; j++)
                {
                    if (arr[j] > arr[j + 1])
                    {
                        temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                        // arr2[temp] = arr[i];
                        //temp++;

                    }
                }
            }
            Console.WriteLine("sorted array is");
            foreach(int i in arr)
            {
                Console.Write(i+" ");
            }

        }
    }
}
