﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice2
{
        class private_member_inherited_class
        {
            private int num = 10;
            private string s = "siri";
            protected void print()
            {
                Console.WriteLine("number " + num + " name " + s);
            }

        }
        class inherit2 : private_member_inherited_class
    {
            public void print()
            {
                base.print();
                Console.WriteLine("Im in child class");
            }
           
        }
        class main9
        {
            public static void main()
            {
                inherit i = new inherit();
                i.print();
               
            }
        }
 }


