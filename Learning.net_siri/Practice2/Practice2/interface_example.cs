﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice2
{
    interface Cars
    {
        public  int doors();
        public void print();
    }
    class ChildCars : Cars
    {
        public  int doors()
        {
            Console.WriteLine("interface example");
            return 5;
        }

        public void print()
        {
            Console.WriteLine("Im in child cars");
        }


    }
    class Main4
    {
        public static void main(string[] args)
        {

            ChildCars c = new ChildCars();
            Console.WriteLine("child car doors " + c.doors());
            c.print();
        }
    }
}
