﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace practice6
{
    public delegate void MyDelegate(int num);
    public class Publisher
    {
        public event MyDelegate MyEvent;


        public void performSomeActions(int num)
        {
            MyEvent?.Invoke(num);
        }
    }


    public class Subscriber
    {
        public void Subscribe(Publisher publisher)
        {
            publisher.MyEvent += HandleEvent;
            
        }


        public void HandleEvent(int num)
        {
            Console.WriteLine("Event handled: " + num++);


        }
    }
    class Events_example
    {
        static void main(string[] args)
        {
            Publisher pub = new Publisher();
            Subscriber subscriber = new Subscriber();


            subscriber.Subscribe(pub);


            pub.performSomeActions(10);
        }
    }
}

