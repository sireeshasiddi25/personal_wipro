﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice6
{
    public delegate void DepartmentChangedEventHandler(object sender, 
        DepartmentChangedEventArgs e);
    public delegate void SalaryChangedEventHandler1(object sender,
       SalaryChangedEventArgs1 s);
    public class DepartmentChangedEventArgs : EventArgs
    {
        public string OldDept { get; }
        public string NewDept { get; }


        public DepartmentChangedEventArgs(string oldDep, string newDep)
        {
            OldDept = oldDep;
            NewDept = newDep;
        }
        
    }
    public class SalaryChangedEventArgs1 : EventArgs {
        public decimal oldSal { get; }
        public decimal newSal { get; }  

        public SalaryChangedEventArgs1(decimal oldSal, decimal newSal)
        {
            oldSal = oldSal;
            newSal = newSal;
        }
    }


    public class Employee1
    {
        private string department;


        public event DepartmentChangedEventHandler DeptChanged;

        public event SalaryChangedEventHandler1 SalaryChanged;


        public string Name { get; }
        private decimal sal;
        
        public decimal salary
        {
            get { return sal; }
            set
            {
                if(sal!=value)
                {
                    OnSalaryChanged(sal,value);
                    sal=value;
                }
            }
        }

        public string departments
        {
            get { return department; }
            set
            {
                if (department != value )
                {
                    OnDeptChanged(department, value);
                    department = value;
                }
            }
        }


        public Employee1(string name, string dep,decimal sal)
        {
            Name = name;
            department = dep;
            sal = sal;
        }


        protected virtual void OnDeptChanged(string oldDept, string newDept)
        {
            DeptChanged?.Invoke(this, new DepartmentChangedEventArgs
                (oldDept, newDept));
        }
        protected virtual void OnSalaryChanged(decimal oldSal, decimal newSal)
        {
            SalaryChanged?.Invoke(this, new SalaryChangedEventArgs1
                (oldSal, newSal));
        }
    }


    public class Department_details
    {
        
        public void HandleDeptChanged(object sender, DepartmentChangedEventArgs e)
        {
            Employee1 employee = (Employee1)sender;
            Console.WriteLine($"Department of {employee.Name} " +
                $"changed from {e.OldDept} to {e.NewDept}");
        }
        public void HandleSalaryChanged(object sender, SalaryChangedEventArgs1 s)
        {
            Employee1 employee = (Employee1)sender;
            Console.WriteLine($"Salary of {employee.Name} " +
                $"changed from {s.oldSal} to {s.newSal}");
        }
    }


    public class Event_employee_department
    {
        static void main(string[] args)
        {
            Employee1 e = new Employee1("Amarnath", "Human_resouce",300000m);
            Department_details d = new Department_details();


            e.DeptChanged += d.HandleDeptChanged;
            
            e.departments = "Trainer";


            e.SalaryChanged += d.HandleSalaryChanged;

            e.salary = 4000000m;
        }
    }
}



