﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace practice6
{
     class reflections_example
    {
        static void main(string[] args)
        {
            Type type = typeof(string);
            Console.WriteLine("Type name " + type.Name);

            Console.WriteLine("Methods:");
            foreach(MethodInfo method in type.GetMethods())
            {
                Console.WriteLine(method.Name);
            }
        }

    }
}
