﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace practice6
{
   /*  class Employee2 
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public double Salary { get; set; }


        public void DisplayInfo()
        {
            Console.WriteLine($"Name: {Name}, Age: {Age}, Salary:{Salary}");
        }
    }*/
    public class dynamic_invocation_inspection
    {
        public static void main(string[] args)
        {
            Type type = typeof(Employee);


            Console.WriteLine("Properties of Employee Class");


            foreach (PropertyInfo property in type.GetProperties())
            {
                Console.WriteLine(property.Name);
            }


            Console.WriteLine("\nMethods of Employee class: ");
            foreach (MethodInfo method in type.GetMethods())
            {
                Console.WriteLine(method.Name);
            }
        }
    }
}

