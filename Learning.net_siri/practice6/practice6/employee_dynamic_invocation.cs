﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace practice6
{
    public class Employee3
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public double Salary { get; set; }


        public void DisplayInfo()
        {
            Console.WriteLine($"Name: {Name}, Age: {Age}, Salary:{Salary}");
        }
    }

        class employee_dynamic_invocation
    {
        public static void main(string[] args)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            Type type = assembly.GetType("practice6.Employee3");


            object instance = Activator.CreateInstance(type);


            PropertyInfo nameProp = type.GetProperty("Name");
            nameProp.SetValue(instance, "Amarnath");


            PropertyInfo ageProp = type.GetProperty("Age");
            ageProp.SetValue(instance, 40);


            PropertyInfo salaryProp = type.GetProperty("Salary");
            salaryProp.SetValue(instance, 400000);


            MethodInfo displayMethod = type.GetMethod("DisplayInfo");
            displayMethod.Invoke(instance, null);
        }
    }
}

