﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice6
{
    public class PaymentProcess
    {
        private CreditCardPayment _creditCardPayment;


        public PaymentProcess()
        {
            _creditCardPayment = new CreditCardPayment();
        }


        public void ProcessPayment()
        {
            _creditCardPayment.Process();
        }
    }


    public class CreditCardPayment
    {
        public void Process()
        {
            Console.WriteLine("Processing credit card payment");
        }
    }


    class tightly_coupling_example
    {
        static void main(string[] args)
        {
            PaymentProcess processor = new PaymentProcess();
            processor.ProcessPayment();
        }
    }
    
}
