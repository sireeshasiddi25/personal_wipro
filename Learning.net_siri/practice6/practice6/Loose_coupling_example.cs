﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice6
{
    public interface IPaymentMethod
    {
        void Process();
    }
    public class PaymentProcess1
    {
        private IPaymentMethod _paymentMethod;


        public PaymentProcess1(IPaymentMethod paymentMethod)
        {
            _paymentMethod = paymentMethod;
        }


        public void ProcessPayment()
        {
            _paymentMethod.Process();
        }
    }


    public class CreditCardPayment1 : IPaymentMethod
    {
        public void Process()
        {
            Console.WriteLine("Processing credit card payment");
        }
    }


    class Loose_coupling_example
    {
        static void main(string[] args)
        {
            IPaymentMethod paymentMethod = new CreditCardPayment1();
            PaymentProcess1 processor = new PaymentProcess1(paymentMethod);
            processor.ProcessPayment();
        }
    }
    
}
