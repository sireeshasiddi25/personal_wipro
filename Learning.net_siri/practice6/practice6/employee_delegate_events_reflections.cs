﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace practice6
{
    public class Employee
    {
        public string Name { get; set; }
        public int ID {  get; set; }    
        public decimal Salary { get; set; }
    }
    public class EmployeeEventArgs : EventArgs
    {
        public Employee Employee { get; }
        public EmployeeEventArgs(Employee employee)
        {
            Employee = employee;
        }
    }
    public class EmployeePublisher
    {
        public event EventHandler<EmployeeEventArgs> EmployeeSalaryInRange;
        public void CheckEmployeeSalary(Employee employee)
        {
            if(employee.Salary>=10000000 && employee.Salary<=40000000)
            {
                OnEmployeeSalaryInRange(employee);
            }
        }
        protected virtual void OnEmployeeSalaryInRange(Employee employee)
        {
            EmployeeSalaryInRange?.Invoke(this, new EmployeeEventArgs(employee));
        }
    }
    public class EmployeeSubscriber
    {
        public void OnEmployeeSalaryInRange(object sender, EmployeeEventArgs e)
        {
            Console.WriteLine($"Employee name : {e.Employee.Name}," +
                $" ID: {e.Employee.ID}, Salary: {e.Employee.Salary}");
        }
    }
     class employee_delegate_events_reflections
    {
        public static void main()
        {


            EmployeePublisher publisher = new EmployeePublisher();
            EmployeeSubscriber subscriber = new EmployeeSubscriber();

            publisher.EmployeeSalaryInRange += subscriber.OnEmployeeSalaryInRange;

            Employee emp1 = new Employee { Name = "siri", ID = 1, Salary = 15000000 };
            Employee emp2 = new Employee { Name = "rupa", ID = 2, Salary = 10000000 };
            Employee emp3 = new Employee { Name = "ram", ID = 3, Salary = 9000000 };

            publisher.CheckEmployeeSalary(emp1);
            publisher.CheckEmployeeSalary(emp2);
            publisher.CheckEmployeeSalary(emp3);
        }

    }
}
