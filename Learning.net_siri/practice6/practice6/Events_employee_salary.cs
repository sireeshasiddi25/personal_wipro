﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice6
{
    public delegate void SalaryChangedEventHandler(object sender, SalaryChangedEventArgs e);
    public class SalaryChangedEventArgs : EventArgs
    {
        public decimal OldSalary { get; }
        public decimal NewSalary { get; }


        public SalaryChangedEventArgs(decimal oldSalary, decimal newSalary)
        {
            OldSalary = oldSalary;
            NewSalary = newSalary;
        }
    }


    public class Employee4
    {
        private decimal _sal;


        public event SalaryChangedEventHandler SalaryChanged;


        public string Name { get; }


        public decimal Salary
        {
            get { return _sal; }
            set
            {
                if (_sal != value)
                {
                    OnSalChanged(_sal, value);
                    _sal = value;
                }
            }
        }


        public Employee4(string name, decimal sal)
        {
            Name = name;
            Salary = sal;
        }


        protected virtual void OnSalChanged(decimal oldSalary, decimal newSalary)
        {
            SalaryChanged?.Invoke(this, new SalaryChangedEventArgs(oldSalary, newSalary));
        }
    }


    public class PayrollSystem
    {
        public void HandleSalaryChanged(object sender, SalaryChangedEventArgs e)
        {
            Employee4 employee = (Employee4)sender;
            Console.WriteLine($"Salary of {employee.Name} " +
                $"changed from {e.OldSalary} to {e.NewSalary}");
        }
    }


    public class Events_employee_salary
    {
        static void main(string[] args)
        {
            Employee4 employee = new Employee4("Amarnath", 3000000m);
            PayrollSystem payrollSystem = new PayrollSystem();


            employee.SalaryChanged += payrollSystem.HandleSalaryChanged;


            employee.Salary = 40000000m;
        }
    }
}
