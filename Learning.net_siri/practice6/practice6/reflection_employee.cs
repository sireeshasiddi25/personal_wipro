﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace practice6
{
    public class Employee6
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public double Salary { get; set; }


        public void DisplayInfo()
        {
            Console.WriteLine($"Name: {Name}, Age: {Age}, Salary:{Salary}");
        }

    }

    public class reflection_employee
    {
        public static void main(string[] args)
        {
            Employee6 employee = new Employee6
            {
                Name = "Amarnath",
                Age = 40,
                Salary = 4000000
            };


            Type type = typeof(Employee6);


            MethodInfo method = type.GetMethod("DisplayInfo");
            method.Invoke(employee, null);
        }
    }
   
}
