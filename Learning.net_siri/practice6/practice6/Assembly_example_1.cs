﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace practice6
{

    internal class Assembly_example_1
    {
        static void main(string[] args)
        {
            Assembly assembly = Assembly.LoadFrom("Practice2");


            Type type = assembly.GetType("Practice2.abstract_example");


            object instance = Activator.CreateInstance(type);


            MethodInfo method = type.GetMethod("Main");
            method.Invoke(instance, null);
        }
    }
}

