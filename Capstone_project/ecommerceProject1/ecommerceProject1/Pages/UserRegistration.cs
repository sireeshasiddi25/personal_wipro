﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecommerceProject1.Pages
{
     public class UserRegistration
    {
        public WebDriver driver;
        public UserRegistration(WebDriver driver)
        {
            this.driver = driver;

        }
        public IWebElement CreateAccOnHome => driver.FindElement(By.XPath("/html/body/div[2]/header/div[1]/div/ul/li[3]/a"));
        public IWebElement firstName => driver.FindElement(By.Id("firstname"));
        public IWebElement lastName => driver.FindElement(By.Name("lastname"));
        public IWebElement email => driver.FindElement(By.Name("email"));

        public IWebElement password => driver.FindElement(By.Name("password"));

        public IWebElement confirm_password => driver.FindElement(By.Name("password_confirmation"));
        public IWebElement create_acc_button => driver.FindElement(By.XPath("//*[@id=\"form-validate\"]/div/div[1]/button"));

        public IWebElement createMessage => driver.FindElement(By.XPath("//*[@id=\"maincontent\"]/div[1]/div[2]/div/div/div"));
        public IWebElement lastNameRequired => driver.FindElement(By.XPath("//*[@id=\"lastname-error\"]"));
        public IWebElement firstNameRequied => driver.FindElement(By.XPath("//*[@id=\"firstname-error\"]"));
        public IWebElement emailRequired => driver.FindElement(By.XPath("//*[@id=\"email_address-error\"]"));
        public IWebElement passwordRequired => driver.FindElement(By.XPath("//*[@id=\"password-error\"]"));
        public IWebElement confirmPassRequired => driver.FindElement(By.XPath("//*[@id=\"password-confirmation-error\"]"));
        public IWebElement invalidEmailFormat => driver.FindElement(By.XPath("//*[@id=\"email_address-error\"]"));
        public IWebElement diffPassConPassMsg => driver.FindElement(By.XPath("//*[@id=\"password-confirmation-error\"]"));
        public IWebElement userExitsMsg => driver.FindElement(By.XPath("//*[@id=\"maincontent\"]/div[2]/div[2]/div/div/div"));
        public IWebElement weakPasswordMsg => driver.FindElement(By.XPath("//*[@id=\"password-error\"]"));
        public IWebElement specialCharInFirstName => driver.FindElement(By.XPath("//*[@id=\"maincontent\"]/div[2]/div[2]/div/div/div"));
        public IWebElement SpecialCharInLastName => driver.FindElement(By.XPath("//*[@id=\"maincontent\"]/div[2]/div[2]/div/div/div"));
    
    }
}
