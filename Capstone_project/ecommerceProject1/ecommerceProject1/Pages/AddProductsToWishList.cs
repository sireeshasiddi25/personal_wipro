﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecommerceProject1.Pages
{
    class AddProductsToWishList
    {
        public WebDriver driver;
        public AddProductsToWishList(WebDriver driver)
        {
            this.driver = driver;

        }
        public IWebElement product1 => driver.FindElement(By.XPath("//*[@id=\"maincontent\"]/div[3]/div/div[2]/div[3]/div/div/ol/li[2]/div/a/span/span/img"));
        public IWebElement Product1AddToWishList => driver.FindElement(By.XPath("//*[@id=\"maincontent\"]/div[2]/div/div[1]/div[5]/div/a[1]"));
        public IWebElement Product2 => driver.FindElement(By.XPath("//*[@id=\"maincontent\"]/div[3]/div/div[2]/div[3]/div/div/ol/li[4]/div/a/span/span/img"));

        public IWebElement Product2WishList => driver.FindElement(By.XPath("//*[@id=\"maincontent\"]/div[2]/div/div[1]/div[5]/div/a[1]"));
        public IWebElement UserNamebutton => driver.FindElement(By.XPath("/html/body/div[2]/header/div[1]/div/ul/li[2]"));
        public IWebElement WishList => driver.FindElement(By.XPath("/html/body/div[2]/header/div[1]/div/ul/li[2]/div/ul/li[2]/a"));
        public IWebElement noItemMsg => driver.FindElement(By.XPath("//*[@id=\"wishlist-view-form\"]/div[1]"));
        public string signIn = "/html/body/div[2]/header/div[1]/div/ul/li[2]/a";
        public IWebElement signin => driver.FindElement(By.XPath(signIn));

        public string Email = "//*[@id=\"email\"]";
        public IWebElement email => driver.FindElement(By.XPath(Email));

        public string Password = "//*[@id=\"pass\"]";
        public IWebElement password => driver.FindElement(By.XPath(Password));
        public string signinbutton = "//*[@id=\"send2\"]/span";
        public IWebElement SignInButton => driver.FindElement(By.XPath(signinbutton));
        public IWebElement Product2AddMsg => driver.FindElement(By.XPath("//*[@id=\"maincontent\"]/div[1]/div[2]/div/div/div"));
        public IWebElement product1AddedMsg => driver.FindElement(By.XPath("//*[@id=\"maincontent\"]/div[1]/div[2]/div/div/div"));

        public IWebElement WhatsNew => driver.FindElement(By.XPath("//*[@id=\"ui-id-3\"]"));
        public IWebElement home => driver.FindElement(By.XPath("/html/body/div[2]/div[2]/ul/li[1]/a"));
    }
}

